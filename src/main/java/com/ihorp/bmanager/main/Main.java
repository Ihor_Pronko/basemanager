package com.ihorp.bmanager.main;

import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.service.CreateStopFile;
import com.ihorp.bmanager.service.settings.SettingsService;
import com.ihorp.bmanager.service.thread.ThreadService;
import com.ihorp.bmanager.service.thread.ThreadStarter;

import java.io.File;

/**
 * Main class.
 */
public final class Main {

    /**
     * Private constructor.
     */
    private Main() {}

    public static boolean isWork = true;

    /**
     * Main method.
     * @param args args.
     */
    public static void main(String[] args) {

        File stopFile = new File(SettingsDefault.STOP_PROGRAM_FILE_NAME.getStringValue());
        CreateStopFile.create(stopFile);
        File defaultSettings = new File (SettingsDefault.DEFAULT_SETTINGS_FILE_NAME.getStringValue());
        SettingsService.createExampleSettings(defaultSettings);

        ThreadStarter threadStarter = new ThreadStarter();
        threadStarter.runSettingsThread();
        threadStarter.runGoogleSheetsThread();

        while (isWork) {
            isWork = stopFile.exists();
            if(!defaultSettings.exists()) {
                SettingsService.createExampleSettings(defaultSettings);
            }
            ThreadService.sleep(1);
        }
    }
}
