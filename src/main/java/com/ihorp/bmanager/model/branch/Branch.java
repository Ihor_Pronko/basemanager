package com.ihorp.bmanager.model.branch;

import java.time.LocalDate;
import java.util.Objects;

public class Branch {

    private int numberOfBranch;

    private int codeOfBranch;

    private String cityLocationOfBranch;

    private String addressOfBranch;

    private LocalDate openingDate;

    private LocalDate closeDate;

    private boolean isActive;

    private String fullBranchName;

    private boolean isEmpty;

    public Branch() { }

    public int getNumberOfBranch() {
        return numberOfBranch;
    }

    public int getCodeOfBranch() {
        return codeOfBranch;
    }

    public String getCityLocationOfBranch() {
        return cityLocationOfBranch;
    }

    public String getAddressOfBranch() {
        return addressOfBranch;
    }

    public LocalDate getOpeningDate() {
        return openingDate;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void setNumberOfBranch(int numberOfBranch) {
        this.numberOfBranch = numberOfBranch;
    }

    public void setCodeOfBranch(int codeOfBranch) {
        this.codeOfBranch = codeOfBranch;
    }

    public void setCityLocationOfBranch(String cityLocationOfBranch) {
        this.cityLocationOfBranch = cityLocationOfBranch;
    }

    public void setAddressOfBranch(String addressOfBranch) {
        this.addressOfBranch = addressOfBranch;
    }

    public void setOpeningDate(LocalDate openingDate) {
        this.openingDate = openingDate;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public String getFullBranchName() {
        return fullBranchName;
    }

    public void setFullBranchName(String fullBranchName) {
        this.fullBranchName = fullBranchName;
    }

    public boolean isEmpty() {
        return numberOfBranch == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Branch branch = (Branch) o;
        return numberOfBranch == branch.numberOfBranch &&
                codeOfBranch == branch.codeOfBranch &&
                isActive == branch.isActive &&
                Objects.equals(cityLocationOfBranch, branch.cityLocationOfBranch) &&
                Objects.equals(addressOfBranch, branch.addressOfBranch) &&
                Objects.equals(openingDate, branch.openingDate) &&
                Objects.equals(closeDate, branch.closeDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(/*company,*/ numberOfBranch, codeOfBranch, cityLocationOfBranch, addressOfBranch, openingDate, closeDate, isActive);
    }

    @Override
    public String toString() {
        return "Branch{" +
                ", numberOfBranch=" + numberOfBranch +
                ", codeOfBranch=" + codeOfBranch +
                ", cityLocationOfBranch='" + cityLocationOfBranch + '\'' +
                ", addressOfBranch='" + addressOfBranch + '\'' +
                ", openingDate=" + openingDate +
                ", closeDate=" + closeDate +
                ", isActive=" + isActive +
                '}';
    }
}
