package com.ihorp.bmanager.model.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.service.file.BackupFileService;

import java.io.File;
import java.time.LocalDate;

/**
 * Class of Backup's program PawnExpert.
 */
public class BackupFileImpl implements PawnExpertFile {

    /**
     *
     */
    private int branchCode;

    /**
     *
     */
    private LocalDate dateOfBackup;

    /**
     *
     */
    private File file;

    /**
     *
     */
    private boolean isBackup;

    public BackupFileImpl(File file) {
        if (BackupFileService.isBackup(file)) {
            this.dateOfBackup = findDate(file);
            this.branchCode = findBranchCode(file);
            this.file = file;
            isBackup = true;
        } else {
            isBackup = false;
        }
    }

    /**
     *
     * @return
     */
    public boolean isBackup() {
        return isBackup;
    }

    //TODO Выкинуть ошибку если не определяет код или дату
    @Override
    public int findBranchCode(File file) {
        return BackupFileService.findBranchCode(file);
    }

    //TODO Выкинуть ошибку если не определяет код или дату
    @Override
    public LocalDate findDate(File file) {
        return BackupFileService.findDate(file);
    }

    @Override
    public boolean replace() throws ThreadNotActualException {
        return BackupFileService.replaceBackup(this);
    }

    @Override
    public boolean clean() throws ThreadNotActualException {
        return BackupFileService.cleanBackup();
    }

    @Override
    public int getBranchCode() {
        return branchCode;
    }

    @Override
    public LocalDate getDate() {
        return dateOfBackup;
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BackupFileImpl that = (BackupFileImpl) o;

        if (branchCode != that.branchCode) return false;
        if (isBackup != that.isBackup) return false;
        if (dateOfBackup != null ? !dateOfBackup.equals(that.dateOfBackup) : that.dateOfBackup != null) return false;
        return file != null ? file.equals(that.file) : that.file == null;
    }

    @Override
    public int hashCode() {
        int result = branchCode;
        result = 31 * result + (dateOfBackup != null ? dateOfBackup.hashCode() : 0);
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + (isBackup ? 1 : 0);
        return result;
    }


}
