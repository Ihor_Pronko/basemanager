package com.ihorp.bmanager.model.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.service.file.BadFileService;

import java.io.File;
import java.time.LocalDate;

/**
 *
 */
public class BadFile implements PawnExpertFile {

    private File file;

    public BadFile(File file) {
        this.file = file;
    }


    @Override
    public boolean replace() throws ThreadNotActualException {
        return BadFileService.replaceBadFile(this.getFile());
    }

    /**
     *
     * @return
     */
    @Override
    public boolean clean() {
        return false;
    }

    /**
     *
     * @param file
     * @return
     */
    @Override
    public int findBranchCode(File file) {
        return 0;
    }

    /**
     *
     * @param file
     * @return
     */
    @Override
    public LocalDate findDate(File file) {
        return null;
    }

    /**
     *
     * @param file
     */
    @Override
    public void setFile(File file) {

    }

    @Override
    public int getBranchCode() {
        return 0;
    }

    @Override
    public LocalDate getDate() {
        return null;
    }

    public File getFile() {
        return file;
    }
}
