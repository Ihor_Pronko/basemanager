package com.ihorp.bmanager.model.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.file.BaseFileService;

import java.io.File;
import java.time.LocalDate;

/**
 *
 */
public class BaseFileImpl implements PawnExpertFile {

    /**
     * TODO
     */
    private int branchCode;

    /**
     * TODO
     */
    private LocalDate date;

    /**
     * TODO
     */
    private File file;

    public BaseFileImpl(File file) throws UndefinedFileException {
        branchCode = findBranchCode(file);
        date = findDate(file);
        this.file = file;
    }

    /**
     *
     */
    @Override
    public int findBranchCode(File file) throws UndefinedFileException {
        return BaseFileService.findBranchCode(file);
    }

    /**
     *
     */
    @Override
    public LocalDate findDate(File file) throws UndefinedFileException {
        return BaseFileService.findDateBaseFile(file);
    }

    /**
     *
     */
    @Override
    public boolean replace() throws ThreadNotActualException {
        return BaseFileService.replaceBaseFile(this);
    }

    /**
     *
     */
    //TODO
    @Override
    public boolean clean() {
        return false;
    }

    @Override
    public void setFile(File file) {

    }

    /**
     * Checks is any file are missing
     *
     * @param settings
     */
    public void checkBaseFiles(Settings settings) {
        //todo
    }

    public int getBranchCode() {
        return branchCode;
    }

    public LocalDate getDate() {
        return date;
    }

    public File getFile() {
        return file;
    }
}
