package com.ihorp.bmanager.model.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.service.file.BuhFileService;

import java.io.File;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 */
public class BuhFileImpl implements PawnExpertFile {

    /**
     *
     */
    private int branchCode;

    /**
     *
     */
    private LocalDate dateFile;

    /**
     *
     */
    private int buhFileType;

    /**
     *
     */
    private File file;

    public BuhFileImpl(File file) throws UndefinedFileException {
        if(BuhFileService.isBuhFile(file)) {
            this.branchCode = findBranchCode(file);
            this.dateFile = findDate(file);
            this.buhFileType = findBuhFileType(file);
            this.file = file;
        }
    }

    /**
     *
     */
    public int findBranchCode(File file) throws UndefinedFileException {
        return BuhFileService.findBranchCode(file);
    }

    /**
     *
     * @param file
     * @return
     */
    @Override
    public LocalDate findDate(File file) throws UndefinedFileException {
        return BuhFileService.findDate(file);
    }

    @Override
    public void setFile(File file) {
        this.file = file;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean replace() throws ThreadNotActualException {
        return BuhFileService.replaceBuhFile(this);
    }

    /**
     *
     */
    @Override
    public boolean clean() {
        //TODO
        return false;
    }

    private int findBuhFileType(File file) {
        return Integer.valueOf(file.getName().replaceAll(".*\\.", ""));
    }

    public int getBranchCode() {
        return branchCode;
    }

    public LocalDate getDate() {
        return dateFile;
    }

    public File getFile() {
        return file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BuhFileImpl buhFile = (BuhFileImpl) o;
        return branchCode == buhFile.branchCode
                && buhFileType == buhFile.buhFileType
                && Objects.equals(dateFile, buhFile.dateFile)
                && Objects.equals(file, buhFile.file);
    }
}
