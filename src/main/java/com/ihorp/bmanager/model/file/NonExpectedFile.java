package com.ihorp.bmanager.model.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.service.file.NonExpectedFileService;

import java.io.File;
import java.time.LocalDate;

/**
 *
 */
public class NonExpectedFile implements PawnExpertFile {

    /**
     *
     */
    private File file;

    public NonExpectedFile(File file) {
        this.file = file;
    }

    @Override
    public boolean replace() throws ThreadNotActualException {
        return NonExpectedFileService.replace(this);
    }

    @Override
    public boolean clean() {
        return false;
    }

    @Override
    public int findBranchCode(File file) {
        return 0;
    }

    @Override
    public LocalDate findDate(File file) {
        return null;
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public int getBranchCode() {
        return 0;
    }

    @Override
    public LocalDate getDate() {
        return null;
    }
}
