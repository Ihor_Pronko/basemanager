package com.ihorp.bmanager.model.file;

import com.ihorp.bmanager.model.branch.Branch;
import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.exception.UndefinedFileException;

import java.io.File;
import java.time.LocalDate;

/**
 *
 */
public interface PawnExpertFile {

    /**
     * TODO
     * @return
     */
    boolean replace() throws ThreadNotActualException;

    /**
     * TODO
     * @return
     */
    boolean clean() throws ThreadNotActualException ;

    /**
     * TODO
     * @param file
     */
    int findBranchCode(File file) throws UndefinedFileException;

    /**
     * TODO
     * @param file
     * @return
     */
    LocalDate findDate(File file) throws UndefinedFileException;

    File getFile();

    void setFile(File file);

    int getBranchCode();

    LocalDate getDate();
}
