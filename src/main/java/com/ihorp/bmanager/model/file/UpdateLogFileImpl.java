package com.ihorp.bmanager.model.file;

import com.ihorp.bmanager.model.branch.Branch;
import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.service.branch.BranchService;
import com.ihorp.bmanager.service.file.UpdateLogFileService;

import java.io.File;
import java.time.LocalDate;

/**
 *
 */
public class UpdateLogFileImpl implements PawnExpertFile {

    /**
     *
     */
    private int branchCode;

    /**
     *
     */
    private LocalDate dateFile;

    /**
     *
     */
    private File file;

    public UpdateLogFileImpl(File file) throws UndefinedFileException {
        branchCode = findBranchCode(file);
        dateFile = findDate(file);
        this.file = file;
    }

    @Override
    public boolean replace() throws ThreadNotActualException {
        return UpdateLogFileService.replace(this);
    }

    @Override
    public boolean clean() {
        //TODO
        return false;
    }

    @Override
    public int findBranchCode(File file) throws UndefinedFileException {
        return UpdateLogFileService.findBranchCode(file);
    }

    @Override
    public LocalDate findDate(File file) throws UndefinedFileException {
        return UpdateLogFileService.findDate(file);
    }

    public int getBranchCode() {
        return branchCode;
    }

    public LocalDate getDate() {
        return dateFile;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
