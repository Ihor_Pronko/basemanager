package com.ihorp.bmanager.model.file.regexp;

public enum BackupFileRegexpEnum {

    /**
     * Standard name of backup file without shop module. Example: BackUp_1_000932_161203_195642.bak
     */
    BACKUP_PAWNEXPERT_FILE_REGEXP("^BackUp_1_[\\d]{6}_[\\d]{6}_[\\d]{6}.bak\\.zip$", "^BackUp_1_[\\d]{6}_[\\d]{6}_[\\d]{6}.bak$"),

    /**
     * Standard name of backup file with shop module. Example: BackUp_11_000001_170615_121253.bak
     */
    BACKUP_PAWNEXPERT_FILE_SHOP_REGEXP("^BackUp_11_[\\d]{6}_[\\d]{6}_[\\d]{6}.bak\\.zip$", "^BackUp_11_[\\d]{6}_[\\d]{6}_[\\d]{6}.bak$"),

    /**
     * Name of backup file in old version BaseManager program. Example: 5 2017-04-04.zip
     */
    BACKUP_BASEMANAGER_FILE_OLD_REGEXP("", "^\\d{1,3}\\s\\d{4}\\-\\d{2}\\-\\d{2}\\.zip$"),

    /**
     * Name of backup file in 2.0+ version BaseManager program. Example: Backup №4 (4) Тарутине 2017-06-15.zip
     */
    BACKUP_BASEMANAGER_FILE_NEW_REGEXP("", "^Backup\\s№\\d{1,3}\\s\\(\\d{1,3}\\)\\s.*\\s\\d{4}\\-\\d{2}\\-\\d{2}\\.zip$");

    private String zipRegexp;

    /**
     * Value of regexp.
     */
    private String regexp;



    BackupFileRegexpEnum(String value, String valueTwo) {
        this.zipRegexp = value;
        this.regexp = valueTwo;
    }

    public String getRegexp() {
        return regexp;
    }

    public String getZipRegexp() {
        return zipRegexp;
    }
}
