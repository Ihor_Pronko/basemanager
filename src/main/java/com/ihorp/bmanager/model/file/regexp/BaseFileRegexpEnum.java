package com.ihorp.bmanager.model.file.regexp;

public enum BaseFileRegexpEnum {

    /**
     * Standard name of ".100" file.
     */
    BASE_PAWNEXPERT_FILE_100_REGEXP("^\\d{9}\\.100\\.zip$", "^\\d{9}\\.100$"),

    /**
     * Standard name of ".101" file.
     */
    BASE_PAWNEXPERT_FILE_101_REGEXP("^\\d{9}\\_\\d{6}\\.101\\.zip$", "^\\d{9}\\_\\d{6}\\.101$"),

    /**
     * Standard name of ".102" file.
     */
    BASE_PAWNEXPERT_FILE_102_REGEXP("^\\d{9}\\.102\\.zip$", "^\\d{9}\\.102$");

    private String zipRegexp;

    /**
     * Value of regexp.
     */
    private String regexp;

    BaseFileRegexpEnum(String value, String valueTwo) {
        this.regexp = valueTwo;
        this.zipRegexp = value;
    }

    public String getRegexp() {
        return regexp;
    }

    public String getZipRegexp() {
        return zipRegexp;
    }
}
