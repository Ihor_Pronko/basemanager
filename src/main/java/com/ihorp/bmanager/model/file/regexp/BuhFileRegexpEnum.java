package com.ihorp.bmanager.model.file.regexp;

public enum BuhFileRegexpEnum {

    /**
     * Standard name of ".002" file.
     */
    BUH_PAWNEXPERT_FILE_002_REGEXP("^\\d{9}\\.002\\.zip$", "^\\d{9}\\.002$"),

    /**
     * Standard name of ".003" file.
     */
    BUH_PAWNEXPERT_FILE_003_REGEXP("^\\d{9}\\.003\\.zip$", "^\\d{9}\\.003$"),

    /**
     * Standard name of ".502" file.
     */
    BUH_PAWNEXPERT_FILE_502_REGEXP("^\\d{9}\\.502\\.zip$", "^\\d{9}\\.502$"),

    /**
     * Standard name of ".004" file.
     */
    BUH_PAWNEXPERT_FILE_004_REGEXP("^\\d{9}\\.004\\.zip$", "^\\d{9}\\.004$");

    private String zipRegexp;

    /**
     * Value of regexp.
     */
    private String regexp;


    BuhFileRegexpEnum(String value, String valueTwo) {
        this.regexp = valueTwo;
        this.zipRegexp = value;
    }

    public String getRegexp() {
        return regexp;
    }

    public String getZipRegexp() {
        return zipRegexp;
    }
}
