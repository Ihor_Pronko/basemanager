package com.ihorp.bmanager.model.file.regexp;

public enum UpdateFileRegexpEnum {

    /**
     * Example: up0021_170112.932
     */
    UPDATE_PAWNEXPERT_FILE_REGEXP("^up\\d{4}\\_\\d{6}\\.\\d{3}\\.zip$", "^up\\d{4}\\_\\d{6}\\.\\d{3}$"),

    /**
     * Example: Update 7 2017-02-14.txt
     */
    UPDATE_BASEMANAGER_FILE_REGEXP("^Update\\s\\d{1,3}\\s\\d{4}\\-\\d{2}\\-\\d{2}\\.txt\\.zip$",
            "^Update\\s\\d{1,3}\\s\\d{4}\\-\\d{2}\\-\\d{2}\\.txt$"),

    /**
     * Example: Update №7 (7) Бердянск 2017-02-14.txt
     */
    UPDATE_BASEMANAGER_FILE_NEW_REGEXP("Update\\s№\\d{1,3}\\s\\(\\d{1,3}\\)\\s.*\\s\\d{4}\\-\\d{2}\\-\\d{2}\\.txt\\.zip$",
            "Update\\s№\\d{1,3}\\s\\(\\d{1,3}\\)\\s.*\\s\\d{4}\\-\\d{2}\\-\\d{2}\\.txt$");

    private String zipRegexp;

    /**
     * Value of regexp.
     */
    private String regexp;

    UpdateFileRegexpEnum(String zipRegexp, String regexp) {
        this.zipRegexp = zipRegexp;
        this.regexp = regexp;
    }

    public String getRegexp() {
        return regexp;
    }

    public String getZipRegexp() {
        return zipRegexp;
    }
}
