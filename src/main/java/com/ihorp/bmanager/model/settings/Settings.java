package com.ihorp.bmanager.model.settings;

import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.File;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Settings class.
 */
public class Settings {

    /**
     * Field active/not active settings.
     */
    private boolean active;

    /**
     * File field.
     */
    private File file;

    /**
     * Field with company name.
     */
    private String companyName;

    /**
     * Field with ftp path.
     */
    private Path ftpPath;

    /**
     * Field with storage path.
     */
    private Path storagePath;

    /**
     * Field with how many backups to keep before delete.
     */
    private int howManyBackupsToKeep;

    /**
     * Field with min date which must to keep in archive.
     */
    private int minDateToArchive;

    /**
     * Field with storage backup path.
     */
    private String storageBackupPath;

    /**
     * Field with storage archive path.
     */
    private String storageArchivePath;

    /**
     * Field with buh path.
     */
    private String buhPath;

    /**
     * Field with baseExpert files path.
     */
    private String basePath;

    /**
     * Field with update files path.
     */
    private String updateLogPath;

    /**
     * Field with non recognized files.
     */
    private String ufoFilesPath;

    /**
     * Field with bad files.
     */
    private String badFilesPath;

    /**
     * Field with id of google tables.
     */
    private String spreadSheetId;

    /**
     * Field with time of last update file.
     */
    private long lastUpdate;

    /**
     * Path with not relevance files.
     */
    private String notRelevanceFilePath;

    /**
     * Field to performance.
     */
    private int hashCode;

    public Settings(File file) {
        this.file = file;
        this.active = SettingsService.getIsActive(file);
        this.companyName = SettingsService.getCompanyName(file);
        this.ftpPath = SettingsService.getFtpPath(file);
        this.storagePath = SettingsService.getStoragePath(file);
        this.howManyBackupsToKeep = SettingsService.getHowManyBackupsToKeep(file);
        this.minDateToArchive = SettingsService.getMinDateToArchive(file);
        this.storageBackupPath = SettingsService.getStorageBackupPath(file);
        this.storageArchivePath = SettingsService.getStorageArchivePath(file);
        this.buhPath = SettingsService.getBuhPath(file);
        this.basePath = SettingsService.getBasePath(file);
        this.updateLogPath = SettingsService.getUpdateLogPath(file);
        this.ufoFilesPath = SettingsService.getUfoFilesPath(file);
        this.badFilesPath = SettingsService.getBadFilesPath(file);
        this.spreadSheetId = SettingsService.getSpreadSheetId(file);
        this.notRelevanceFilePath = SettingsService.getNotRelevanceFilePath(file);
        this.lastUpdate = file.lastModified();

        if (this.isActive() && (this.companyName == null || this.ftpPath == null || this.storagePath == null)) {
            this.active = false;
        }
    }

    public boolean isActive() {
        return this.active;
    }

    public File getFile() {
        return this.file;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public void setNonActive() {
        this.active = false;
    }

    public Path getFtpPath() {
        return this.ftpPath;
    }

    public Path getStoragePath() {
        return this.storagePath;
    }

    public int getHowManyBackupsToKeep() {
        return this.howManyBackupsToKeep;
    }

    public int getMinDateToArchive() {
        return this.minDateToArchive;
    }

    public String getStorageBackupPath() {
        return this.storageBackupPath;
    }

    public String getStorageArchivePath() {
        return this.storageArchivePath;
    }

    public String getBuhPath() {
        return this.buhPath;
    }

    public String getBasePath() {
        return this.basePath;
    }

    public String getUpdateLogPath() {
        return this.updateLogPath;
    }

    public String getUfoFilesPath() {
        return this.ufoFilesPath;
    }

    public String getBadFilesPath() {
        return this.badFilesPath;
    }

    public String getSpreadSheetId() {
        return this.spreadSheetId;
    }

    public long getLastUpdate() {
        return this.lastUpdate;
    }

    public String getNotRelevanceFilePath() {
        return notRelevanceFilePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Settings settings = (Settings) o;
        return active == settings.active
                && howManyBackupsToKeep == settings.howManyBackupsToKeep
                && minDateToArchive == settings.minDateToArchive
                && lastUpdate == settings.lastUpdate
                && hashCode == settings.hashCode
                && Objects.equals(file, settings.file)
                && Objects.equals(companyName, settings.companyName)
                && Objects.equals(ftpPath, settings.ftpPath)
                && Objects.equals(storagePath, settings.storagePath)
                && Objects.equals(storageBackupPath, settings.storageBackupPath)
                && Objects.equals(storageArchivePath, settings.storageArchivePath)
                && Objects.equals(buhPath, settings.buhPath)
                && Objects.equals(basePath, settings.basePath)
                && Objects.equals(updateLogPath, settings.updateLogPath)
                && Objects.equals(ufoFilesPath, settings.ufoFilesPath)
                && Objects.equals(badFilesPath, settings.badFilesPath)
                && Objects.equals(spreadSheetId, settings.spreadSheetId);
    }

    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            this.hashCode = Objects.hash(active, file, companyName, ftpPath, storagePath, howManyBackupsToKeep,
                    minDateToArchive, storageBackupPath, storageArchivePath, buhPath, basePath, updateLogPath,
                    ufoFilesPath, badFilesPath, spreadSheetId, lastUpdate, hashCode);
        }
        return this.hashCode;
    }
}
