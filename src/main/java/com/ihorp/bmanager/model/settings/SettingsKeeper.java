package com.ihorp.bmanager.model.settings;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;

import java.util.HashMap;
import java.util.Map;

public class SettingsKeeper {

    private static SettingsKeeper instance;

    private HashMap<Integer, Settings> settings;

    private SettingsKeeper() {
        settings = new HashMap<>();
    }

    public static synchronized SettingsKeeper getInstance() {
        if (instance == null) {
            instance = new SettingsKeeper();
        }
        return instance;
    }

    public synchronized Settings getActiveCompanySettings(String company) {
        return this.settings.entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(Settings::isActive)
                .filter(o -> o.getCompanyName().equals(company)).findFirst().orElse(null);
    }

    public synchronized Settings addCompanySettings(Settings settings) {
        if (!this.settings.containsKey(settings.hashCode())) {
            return this.settings.put(settings.hashCode(), settings);
        }
        return null;
    }

    public synchronized boolean deleteCompanySettings(Settings settings) {
        if (this.settings.containsKey(settings.hashCode())) {
            this.settings.remove(settings.hashCode());
            return true;
        }
        return false;
    }

    public boolean matchCompany(int hashCode) {
        if (settings.entrySet().stream()
                .map(Map.Entry::getValue)
                .anyMatch(o -> o.hashCode() == hashCode)) {
            return true;
        }
        return false;
    }

    /**
     * @param company
     * @return
     */
    public boolean matchActiveCompany(String company) {
        if (settings.entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(Settings::isActive)
                .anyMatch(o -> o.getCompanyName().equals(company))) {
            return true;
        }
        return false;
    }

    public HashMap<Integer, Settings> getSettings() {
        return this.settings;
    }
}
