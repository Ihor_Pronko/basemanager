package com.ihorp.bmanager.model.settings.settingsEnum;

/**
 * Enum SettingsDefault.
 */
public enum SettingsDefault {

    /**
     * How many backups to keep in Actual before delete.
     */
    HOW_MANY_BACKUPS_TO_KEEP(7, ""),

    /**
     * Minimum date of each month to archive.
     */
    MIN_DATE_TO_ARCHIVE(2, ""),

    /**
     * Name of backup path.
     */
    BACKUP_PATH_NAME(0, "BackupPE_ACTUAL"),

    /**
     * Name of archive path.
     */
    BACKUP_ARCHIVE_PATH_NAME(0, "BackupPE_ARCHIVE"),

    /**
     * Name of default path.
     */
    BUH_PATH(0, "Buh_"),

    /**
     *  Name of path for BaseExpert.
     */
    BASE_PATH(0, "BaseExpert_DirIn"),

    /**
     * Name of path for updates log files.
     */
    UPDATE_LOG_PATH(0, "UpdatesPE_Logs"),

    /**
     * Name of path for non recognized files.
     */
    UFO_FILES_PATH(0, "Unrecognized_Files"),

    /**
     * Name of path for bad files.
     */
    BAD_FILES_PATH(0, "Bad_Files"),

    /**
     * How often update sheets in ms.
     */
    HOW_OFTEN_UPDATE_SHEETS(30, ""),

    /**
     * Pattern to check path in windows file system.
     */
    PATH_PATTERN_WINDOWS(0, "^[A-Z]{1}:\\\\.*$"),

    /**
     * Pattern to check path in unix file system.
     */
    PATH_PATTERN_UNIX(0, "^/.*$"),

    /**
     * Error message if FTP path not valid.
     */
    FTP_PATH_ERROR_MESSAGE(0, "0, Ftp path must match pattern " + PATH_PATTERN_WINDOWS.getStringValue() + " or "
    + PATH_PATTERN_UNIX.getStringValue()),

    /**
     * Error message if Storage path not valid.
     */
    STORAGE_PATH_ERROR_MESSAGE(0, "0, Storage path must match pattern " + PATH_PATTERN_WINDOWS.getStringValue() + " or "
            + PATH_PATTERN_UNIX.getStringValue()),

    /**
     * Default settings example file name.
     */
    DEFAULT_SETTINGS_FILE_NAME(0, "Settings_-Company-.properties"),

    /**
     * File to stop program file name.
     */
    STOP_PROGRAM_FILE_NAME(0, "DeleteToStop"),

    /**
     * Name of thread of settings update.
     */
    SETTINGS_THREAD_NAME(0, "SettingsThread"),

    /**
     * Google sheets thread name.
     */
    GOOGLE_SHEETS_THREAD_NAME(0, "Google_sheets_thread"),

    /**
     * Comment in settings file.
     */
    COMMENT_IN_SETTINGS_FILE(0, "# 1) Для того что бы настройки были активны, выставляется 1 или true."
            + "\n# 2) Обяательное поле. Название компании должно точно совпадать с названием папки на FTP сервере."
            + "\n# 3) Обяательное поле. Полный путь к папке FTP до места распределения на компании. Обратные слешы должны "
            + "экранироваться, пример: F:\\\\FTP."
            + "\n# 4) Обяательное поле. Полный путь к папке хранилища, подпапки с названиями компаний будут созданны "
            + "автоматически, тоже с двойными слешами. Пример: F:\\\\Storage."
            + "\n# 5) Количество хранимых бекапов. По умолчанию 7, минимум 1."
            + "\n# 6) Дата каждого мецяса что уходит на хранение, если данного числа нет, уходит ближайшая."
            + "\n# 7) ID google таблиц с которых будут считываться данные."
            + "\n# 8) Для переименования старых бекапов удалите значение \"0\""),

    /**
     * Path with not relevance files.
     */
    NOT_RELEVANCE_PATH(0, "Not_Relevance_Files"),

    /**
     * Error to write in settings.
     */
    GOOGLE_SHEETS_ID_ERROR_MESSAGE(0, "Error. This ID not found: ");

    /**
     * Field for integer value of enum.
     */
    private int integerValue;

    /**
     * Field for string value of enum.
     */
    private String stringValue;

    SettingsDefault(int integerValue, String stringValue) {
        this.integerValue = integerValue;
        this.stringValue = stringValue;
    }

    public int getIntegerValue() {
        return this.integerValue;
    }

    public String getStringValue() {
        return this.stringValue;
    }
}
