package com.ihorp.bmanager.model.settings.settingsEnum;

/**
 * SettingsField class.
 */
public enum SettingsField {

    /**
     * Property ACTIVE_SETTINGS.
     */
    ACTIVE_SETTINGS("1*_Active_Settings", "0"),

    /**
     * Property COMPANY_NAME.
     */
    COMPANY_NAME("2*_Company_Name", "Company"),

    /**
     * Property FTP_PATH.
     */
    FTP_PATH("3*_FTP_Path", "F:\\FTP"),

    /**
     * Property STORAGE_PATH.
     */
    STORAGE_PATH("4*_Storage_Path", "F:\\"),

    /**
     * Property HOW_MANY_BACKUPS_TO_KEEP.
     */
    HOW_MANY_BACKUPS_TO_KEEP("5_How_many_backups_keep", "7"),

    /**
     * Property MIN_DATE_TO_ARCHIVE.
     */
    MIN_DATE_TO_ARCHIVE("6_Min_date_to_archive", "2"),

    /**
     *  Property ID_GOOGLE_SHEET.
     */
    ID_GOOGLE_SHEET("7_ID_of_google_sheet", ""),

    /**
     *
     */
    RENAME_OLD_BACKUPS("8_Rename_old_backups_in_storage", "0");

    /**
     * Field with string value.
     */
    private String parameter;

    /**
     * Field with example property.
     */
    private String exampleProperty;

    SettingsField(String field, String exampleProperty) {
        this.parameter = field;
        this.exampleProperty = exampleProperty;
    }

    public String getParameter() {
        return this.parameter;
    }

    public String getExampleProperty() {
        return this.exampleProperty;
    }
}