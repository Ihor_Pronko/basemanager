package com.ihorp.bmanager.model.thread;

import com.ihorp.bmanager.main.Main;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.service.branch.BranchService;
import com.ihorp.bmanager.service.thread.ThreadService;

import java.time.LocalDateTime;

public class GoogleSheetsThread extends Thread {

    private String settingsMonitor;
    private String sheetsMonitor;

    public GoogleSheetsThread(String settingsMonitor, String sheetsMonitor) {
        super(SettingsDefault.GOOGLE_SHEETS_THREAD_NAME.getStringValue());
        this.settingsMonitor = settingsMonitor;
        this.sheetsMonitor = sheetsMonitor;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + ": started. :" + LocalDateTime.now()); //TODO Del
        BranchService branchService = new BranchService();
        while (Main.isWork) {
            ThreadService.waitThread(settingsMonitor);
            branchService.appointBranchesToSettings();
            System.out.println(Thread.currentThread().getName() + ": Branches added. :" + LocalDateTime.now()); //TODO Del
            ThreadService.notifyAllThreads(sheetsMonitor);
            ThreadService.sleep(SettingsDefault.HOW_OFTEN_UPDATE_SHEETS.getIntegerValue());
        }
    }


}
