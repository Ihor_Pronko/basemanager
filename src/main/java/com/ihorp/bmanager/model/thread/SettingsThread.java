package com.ihorp.bmanager.model.thread;

import com.ihorp.bmanager.main.Main;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.service.settings.updates.SettingsUpdates;
import com.ihorp.bmanager.service.thread.ThreadService;

import java.time.LocalDateTime;

public class SettingsThread extends Thread {

    private String monitor;

    public SettingsThread(String monitor) {
        super(SettingsDefault.SETTINGS_THREAD_NAME.getStringValue());
        this.monitor = monitor;
    }

    /**
     *
     */
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " started. " + LocalDateTime.now()); //TODO Del
        SettingsUpdates settingsUpdates = new SettingsUpdates();
        while (Main.isWork) {
            settingsUpdates.checkSettingsUpdates();
            ThreadService.notifyAllThreads(monitor);
            ThreadService.sleep(2);
        }
        //To log "Thread * stopped"
    }
}
