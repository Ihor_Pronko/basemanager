package com.ihorp.bmanager.model.thread.fileThread;

import com.ihorp.bmanager.main.Main;
import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.service.file.BackupFileService;
import com.ihorp.bmanager.service.thread.ThreadService;

import java.time.LocalDateTime;

public class CleanFilesThread implements Runnable {

    private String moveFileMonitor;

    public CleanFilesThread(String moveFileMonitor) {
        this.moveFileMonitor = moveFileMonitor;
    }

    @Override
    public void run() {
        ThreadService.waitThread(moveFileMonitor);
        System.out.println(Thread.currentThread().getName() + " Starts" + LocalDateTime.now()); //TODO Del
        boolean actual = true;
        while(Main.isWork && actual) {
            try {
                BackupFileService.cleanBackup();
            } catch (ThreadNotActualException e) {
                actual = false;
            }
            ThreadService.sleep(1);
        }
        System.out.println(Thread.currentThread().getName() + " is stopped"); //TODO Del
    }
}
