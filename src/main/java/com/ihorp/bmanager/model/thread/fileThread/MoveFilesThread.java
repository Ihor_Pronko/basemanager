package com.ihorp.bmanager.model.thread.fileThread;

import com.ihorp.bmanager.main.Main;
import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.settings.SettingsKeeper;
import com.ihorp.bmanager.service.file.IdentifyFile;
import com.ihorp.bmanager.service.file.ListFiles;
import com.ihorp.bmanager.service.file.checkFiles.CheckFile;
import com.ihorp.bmanager.service.settings.SettingsService;
import com.ihorp.bmanager.service.thread.ThreadService;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MoveFilesThread implements Runnable {

    /**
     *
     */
    private String googleSheetsMonitor;

    /**
     *
     */
    private String moveFileMonitor;

    public MoveFilesThread(String googleSheetsMonitor, String moveFileMonitor) {
        this.googleSheetsMonitor = googleSheetsMonitor;
        this.moveFileMonitor = moveFileMonitor;
    }

    @Override
    public void run() {
        ThreadService.waitThread(googleSheetsMonitor);
        boolean actual = true;
        System.out.println(Thread.currentThread().getName() + " Starts" + LocalDateTime.now()); //TODO Del
        while (Main.isWork && actual) {
            try {
                moveFiles(listFilesForMove());
            } catch (ThreadNotActualException e) {
                actual = false;
            }
            ThreadService.notifyAllThreads(moveFileMonitor);
            ThreadService.sleep(10);
        }
        System.out.println(Thread.currentThread().getName() + " Stopped"); //TODO Del
    }

    /**
     * @return
     */
    private List<File> listFilesForMove() throws ThreadNotActualException {
        List<File> files = new ArrayList<>();
        files.addAll(ListFiles.recursive(SettingsService.getSettingsFromThreadName().getFtpPath().toString())
                .filter(y -> y.getAbsolutePath().matches(".*[/\\\\]In[/\\\\].*"))
                .collect(Collectors.toList()));
        files.addAll(new ArrayList<>(ListFiles.inCurrentPath(SettingsService.getSettingsFromThreadName()
                .getNotRelevanceFilePath())));
        return files;
    }

    /**
     * @param files
     */
    private void moveFiles(List<File> files) throws ThreadNotActualException {
        List<PawnExpertFile> peFiles = files.stream()
                .filter(y -> !y.getName().matches(".*\\.tmp$"))
                .filter(y -> !y.getName().matches("^\\..*|^_.*"))
                .map(IdentifyFile::identifyFile)
                .filter(y -> y != null && y.getFile().exists())
                .collect(Collectors.toList());
        for (PawnExpertFile peFile : peFiles) {
            peFile.replace();
        }
    }
}

