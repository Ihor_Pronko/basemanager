package com.ihorp.bmanager.service;

import java.io.File;
import java.io.IOException;

/**
 * Service class to create new file.
 */
public final class CreateStopFile {

    /**
     * Default constructor.
     */
    private CreateStopFile() {
    }

    /**
     * Create a file in root Path.
     * @return true of false.
     */
    public static boolean create(File stopFile) {
        try {
            stopFile.createNewFile();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            // Add logger.
            return false;
        }
    }
}
