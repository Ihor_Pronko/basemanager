package com.ihorp.bmanager.service.branch;

import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.dao.googleSheets.BranchKeeper;
import com.ihorp.bmanager.model.branch.Branch;
import com.ihorp.bmanager.model.settings.SettingsKeeper;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.service.sheets.SheetServices;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BranchService {

    public BranchService() {}

    public void appointBranchesToSettings() {

        Set<Map.Entry<Integer, Settings>> set;
        set = SettingsKeeper.getInstance().getSettings().entrySet();
        set.stream().map(Map.Entry::getValue)
                .filter(o -> o.isActive() && !o.getSpreadSheetId().equals("")
                        && !o.getSpreadSheetId().startsWith(SettingsDefault.GOOGLE_SHEETS_ID_ERROR_MESSAGE.getStringValue()))
                .forEach(BranchService::setBranches);
    }

    /**
     *
     * @param settings
     */
    private static void setBranches(Settings settings) {
        List<Branch> branches = BranchService.getBranchesFromGoogle(settings);
        if (branches != null && !branches.isEmpty()) {
            BranchKeeper.setBranches(settings.getCompanyName(), branches);
        }
    }

    /**
     *
     * @param settings
     * @return
     */
    private static List<Branch> getBranchesFromGoogle(Settings settings) {
        String range = "Лист1!A2:Z";
        List<List<Object>> values = SheetServices.getSheet(settings, range);
        if (values.isEmpty()) {
            return null;
        }
        return convertListOfStringsToBranch(values);
    }

    /**
     *
     * @param stringBranches
     * @return
     */
    private static List<Branch> convertListOfStringsToBranch(List<List<Object>> stringBranches) {

        List<Branch> branches = new ArrayList<>();
        for (List<Object> branchFields : stringBranches) {
            int size = branchFields.size();
            if (size > 4 && parseDate(branchFields.get(4).toString()).matches(".*\\-.*")) {
                Branch branch = new Branch();
                branch.setNumberOfBranch(parseInt(branchFields.get(0).toString()));
                branch.setCodeOfBranch(parseInt(branchFields.get(1).toString()));
                branch.setCityLocationOfBranch(branchFields.get(2).toString());
                branch.setAddressOfBranch(branchFields.get(3).toString());
                branch.setOpeningDate(LocalDate.parse(parseDate(branchFields.get(4).toString())));
                branch.setCloseDate(LocalDate.parse(closeDate(branchFields)));
                branch.setActive(isActive(branchFields));
                branch.setFullBranchName(fullBranchName(branch));
                if (!isActiveDuplicate(branches, branch)) {
                    branches.add(branch);
                }
            }
        }
        return branches;
    }

    /**
     * Find a integer value in string.
     *
     * @param value string value.
     * @return integer value.
     */
    private static int parseInt(String value) {
        if (value == null || value.equals("") || !value.matches("^[1-9]+.*")) {
            return 0;
        }
        Matcher matcher = Pattern.compile("[1-9][0-9]*").matcher(value);
        matcher.find();
        return Integer.parseInt(matcher.group());
    }

    /**
     *
     * @param stringDate
     * @return
     */
    private static String parseDate(String stringDate) {
        if (stringDate.matches("^\\d{2}[.,/\\-]\\d{2}[.,/\\-]\\d{4}$")) {
            int day = Integer.parseInt(stringDate.substring(0, 2));
            int month = Integer.parseInt(stringDate.substring(3, 5));
            int year = Integer.parseInt(stringDate.substring(6, 10));
            return getStringDate(year, month, day);
        } else if (stringDate.matches("^\\d{4}[.,/\\-]\\d{2}[.,/\\-]\\d{2}$")) {
            int day = Integer.parseInt(stringDate.substring(8, 10));
            int month = Integer.parseInt(stringDate.substring(5, 7));
            int year = Integer.parseInt(stringDate.substring(0, 4));
            return getStringDate(year, month, day);
        } else if (stringDate.matches("")) {
            return "empty";
        } else {
            return "error";
        }
    }

    /**
     *
     * @param year
     * @param month
     * @param day
     * @return
     */
    private static String getStringDate(int year, int month, int day) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.valueOf(year));
        stringBuilder.append("-");
        if (month < 10) {
            stringBuilder.append("0");
            stringBuilder.append(String.valueOf(month));
        } else {
            stringBuilder.append(String.valueOf(month));
        }
        stringBuilder.append("-");
        if (day < 10) {
            stringBuilder.append("0");
            stringBuilder.append(String.valueOf(day));
        } else {
            stringBuilder.append(String.valueOf(day));
        }
        return stringBuilder.toString();
    }

    /**
     *
     * @param fields
     * @return
     */
    private static String closeDate(List<Object> fields) {
        if (fields.size() < 6 || parseDate(fields.get(5).toString()).matches("empty")) {
            return "2222-11-11";
        } else if (parseDate(fields.get(5).toString()).matches("error")) {
            return "2222-11-11";
        } else {
            return parseDate(fields.get(5).toString());
        }
    }

    /**
     *
     * @param fields
     * @return
     */
    private static boolean isActive(List<Object> fields) {
        if (fields.size() < 6 || parseDate(fields.get(5).toString()).matches("empty")) {
            return true;
        } else if (parseDate(fields.get(5).toString()).matches("error")) {
            return false;
        } else if (LocalDate.parse(parseDate(fields.get(5).toString())).isAfter(LocalDate.now())) {
            return true;
        } else if (LocalDate.parse(parseDate(fields.get(5).toString())).isBefore(LocalDate.now())) {
            return false;
        }
        return false;
    }

    /**
     *
     * @param listBranches
     * @param branch
     * @return
     */
    private static boolean isActiveDuplicate(List<Branch> listBranches, Branch branch) {
        return listBranches.stream()
                .filter(Branch::isActive)
                .anyMatch(o -> o.getCodeOfBranch() == branch.getCodeOfBranch());
    }

    /**
     *
     * @param branch
     * @return
     */
    private static String fullBranchName(Branch branch) {
        return "№" + String.valueOf(branch.getNumberOfBranch())
                + " ("
                + branch.getCodeOfBranch()
                + ") "
                + branch.getCityLocationOfBranch();
    }
}
