package com.ihorp.bmanager.service.dao;

import com.ihorp.bmanager.model.branch.Branch;
import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.file.PawnExpertFile;

public interface DAO {

    /**
     *
     * @param pawnExpertFile
     * @return
     */
    Branch getBranch(PawnExpertFile pawnExpertFile) throws ThreadNotActualException;

    boolean matchCompany() throws ThreadNotActualException ;
}
