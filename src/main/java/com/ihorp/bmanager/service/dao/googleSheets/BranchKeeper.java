package com.ihorp.bmanager.service.dao.googleSheets;

import com.ihorp.bmanager.model.branch.Branch;
import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.service.settings.SettingsService;
import com.ihorp.bmanager.service.thread.ThreadService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BranchKeeper {

    /**
     *
     */
    private static BranchKeeper instance;

    /**
     *
     */
    private static HashMap<String, List<Branch>> companies = new HashMap<>();

    private BranchKeeper() {
    }

    /**
     *
     * @return
     */
    protected static synchronized BranchKeeper getInstance() {
        if (instance == null) {
            instance = new BranchKeeper();
        }
        return instance;
    }

    /**
     *
     *
     * @param companyName
     * @param branches
     */
    public static synchronized void setBranches(String companyName, List<Branch> branches) {
        if(companyName != null && !companyName.matches("") && branches != null && !branches.isEmpty()) {
            companies.put(companyName, branches);
        }
    }

    protected synchronized List<Branch> getAllBranches() throws ThreadNotActualException {
        String company = SettingsService.getSettingsFromThreadName().getCompanyName();
        if (companies.containsKey(company)) {
            return companies.get(company);
        }
        return null;
    }
}
