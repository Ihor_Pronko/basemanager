package com.ihorp.bmanager.service.dao.googleSheets;

import com.ihorp.bmanager.model.branch.Branch;
import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.file.BadFile;
import com.ihorp.bmanager.model.file.NonExpectedFile;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.service.dao.DAO;

import java.util.List;

public class GoogleSheetsDAO implements DAO {

    private static GoogleSheetsDAO instance;

    private GoogleSheetsDAO() {}

    public static GoogleSheetsDAO getInstance() {
        if(instance == null) {
            return new GoogleSheetsDAO();
        }
        return instance;
    }

    @Override
    public Branch getBranch(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {

        List<Branch> branches = BranchKeeper.getInstance().getAllBranches();

        if (branches == null || pawnExpertFile instanceof NonExpectedFile
                || pawnExpertFile instanceof BadFile || pawnExpertFile.getBranchCode() == 0) {
            return new Branch();
        }

        return branches.stream()
                .filter(o -> o.getCodeOfBranch() == pawnExpertFile.getBranchCode()
                        && o.getOpeningDate().isBefore(pawnExpertFile.getDate())
                        && o.getCloseDate().isAfter(pawnExpertFile.getDate()))
                .findFirst().orElse(null);
    }

    /**
     * Works only from file threads!!!
     *
     * @return
     */
    @Override
    public boolean matchCompany() throws ThreadNotActualException {
        return BranchKeeper.getInstance().getAllBranches() != null;
    }
}
