package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.service.dao.googleSheets.BranchKeeper;
import com.ihorp.bmanager.model.file.BackupFileImpl;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.file.regexp.BackupFileRegexpEnum;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.branch.BranchService;
import com.ihorp.bmanager.service.dao.googleSheets.GoogleSheetsDAO;
import com.ihorp.bmanager.service.file.checkFiles.CheckFile;
import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BackupFileService {

    /**
     * @param file
     * @return
     */
    public static boolean isBackup(File file) {
        for (BackupFileRegexpEnum regexp : BackupFileRegexpEnum.values()) {
            if (file.getName().matches(regexp.getRegexp())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     *
     * @param file
     * @return
     */
    public static LocalDate findDate(File file) {
        if (file == null) {
            return null;
        }
        if (file.getName().matches(BackupFileRegexpEnum.BACKUP_BASEMANAGER_FILE_OLD_REGEXP.getRegexp()) ||
                file.getName().matches(BackupFileRegexpEnum.BACKUP_BASEMANAGER_FILE_NEW_REGEXP.getRegexp())) {
            return dateFromBMFile(file.getName());
        }
        if (file.getName().matches(BackupFileRegexpEnum.BACKUP_PAWNEXPERT_FILE_REGEXP.getRegexp())) {
            return dateFromNonShopPEFile(file.getName());
        }
        if (file.getName().matches(BackupFileRegexpEnum.BACKUP_PAWNEXPERT_FILE_SHOP_REGEXP.getRegexp())) {
            return dateFromShopPEFile(file.getName());
        }
        return LocalDate.of(1900, 01, 01);
    }

    private static LocalDate dateFromBMFile(String fileName) {
        String date = fileName.replaceAll("^.*\\s", "");
        return LocalDate.of(Integer.valueOf(date.substring(0, 4)),
                Integer.valueOf(date.substring(5, 7)),
                Integer.valueOf(date.substring(8, 10)));
    }

    private static LocalDate dateFromShopPEFile(String fileName) {
        return LocalDate.of(Integer.valueOf("20" + fileName.substring(17, 19)),
                Integer.valueOf(fileName.substring(19, 21)),
                Integer.valueOf(fileName.substring(21, 23)));
    }

    private static LocalDate dateFromNonShopPEFile(String fileName) {
        return LocalDate.of(Integer.valueOf("20" + fileName.substring(16, 18)),
                Integer.valueOf(fileName.substring(18, 20)),
                Integer.valueOf(fileName.substring(20, 22)));
    }

    /**
     * TODO
     *
     * @param file
     * @return
     */
    public static int findBranchCode(File file) {
        if (file == null) {
            return 0;
        }

        if (file.getName().matches(BackupFileRegexpEnum.BACKUP_PAWNEXPERT_FILE_SHOP_REGEXP.getRegexp()) ||
                file.getName().matches(BackupFileRegexpEnum.BACKUP_PAWNEXPERT_FILE_REGEXP.getRegexp())) {
            return codeFromPEFile(file.getName());
        } else if (file.getName().matches(BackupFileRegexpEnum.BACKUP_BASEMANAGER_FILE_NEW_REGEXP.getRegexp())) {
            return codeFromNewBMFile(file.getName());
        } else if (file.getName().matches(BackupFileRegexpEnum.BACKUP_BASEMANAGER_FILE_OLD_REGEXP.getRegexp())) {
            return codeFromOldBMFile(file.getName());
        }
        return 0;
    }

    private static int codeFromNewBMFile(String fileName) {
        return Integer.valueOf(fileName.replaceAll(".*\\(", "").replaceAll("\\).*", ""));
    }

    private static int codeFromOldBMFile(String filename) {
        return Integer.valueOf(filename.replaceAll("\\s.*", ""));
    }

    private static int codeFromPEFile(String fileName) {
        return Integer.valueOf(fileName.replaceAll("^BackUp_\\d{1,2}_", "").replaceAll("_\\d+_\\d+.bak$", ""));
    }


    /**
     * @param
     * @return
     */
    public static boolean replaceBackup(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {

        String absoluteBackupFileName = getAbsoluteBackupName(pawnExpertFile);

        CheckFile.moveIfNotRelevance(pawnExpertFile);

        createPathToFile(absoluteBackupFileName);

        moveToArchive(pawnExpertFile);

        return FileService.moveFile(Paths.get(pawnExpertFile.getFile().toURI()), Paths.get(absoluteBackupFileName));
    }

    /**
     * @param pawnExpertFile
     * @return
     */
    private static String getAbsoluteBackupName(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        GoogleSheetsDAO googleSheetsDAO = GoogleSheetsDAO.getInstance();
        if (googleSheetsDAO.matchCompany() &&
                googleSheetsDAO.getBranch(pawnExpertFile).getCodeOfBranch() != 0) {
            return SettingsService.getSettingsFromThreadName().getStorageBackupPath()
                    + File.separator + googleSheetsDAO.getBranch(pawnExpertFile).getFullBranchName()
                    + File.separator + backupFileName(pawnExpertFile);
        } else {
            return SettingsService.getSettingsFromThreadName().getStorageBackupPath()
                    + File.separator + pawnExpertFile.getBranchCode()
                    + File.separator + backupFileName(pawnExpertFile);
        }
    }

    private static String backupFileName(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        GoogleSheetsDAO googleSheetsDAO = GoogleSheetsDAO.getInstance();
        if (googleSheetsDAO.matchCompany() &&
                googleSheetsDAO.getBranch(pawnExpertFile).getCodeOfBranch() != 0) {
            return "Backup " + googleSheetsDAO.getBranch(pawnExpertFile).getFullBranchName()
                    + " " + pawnExpertFile.getDate().toString() + ".zip";
        } else {
            return pawnExpertFile.getBranchCode() + " " + pawnExpertFile.getDate().toString() + ".zip";
        }
    }

    /**
     * @param pawnExpertFile
     */
    private static void moveToArchive(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {

        String absoluteArchiveFileName = getArchiveAbsoluteFileName(pawnExpertFile);

        Settings settings = SettingsService.getSettingsFromThreadName();
        if (Arrays.stream(createPathToFile(absoluteArchiveFileName).listFiles())
                .map(BackupFileImpl::new)
                .filter(BackupFileImpl::isBackup)
                .filter(o -> o.getDate().getDayOfMonth() >= settings.getMinDateToArchive())
                .noneMatch(o -> o.getDate().getDayOfMonth() <= pawnExpertFile.getDate().getDayOfMonth())
                && pawnExpertFile.getDate().getDayOfMonth() >= settings.getMinDateToArchive()) {
            FileService.copyFile(pawnExpertFile.getFile().toPath(),
                    new File(getArchiveAbsoluteFileName(pawnExpertFile)).toPath());
        }
    }

    /**
     * @param pawnExpertFile
     * @return
     */
    public static String getArchiveAbsoluteFileName(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        GoogleSheetsDAO googleSheetsDAO = GoogleSheetsDAO.getInstance();
        LocalDate dateBackup = pawnExpertFile.getDate();
        if (googleSheetsDAO.matchCompany() &&
                googleSheetsDAO.getBranch(pawnExpertFile).getCodeOfBranch() != 0) {
            return SettingsService.getSettingsFromThreadName().getStorageArchivePath()
                    + File.separator + dateBackup.getYear()
                    + File.separator + dateBackup.getMonthValue() + "_" + dateBackup.getMonth().toString()
                    + File.separator + googleSheetsDAO.getBranch(pawnExpertFile).getFullBranchName()
                    + File.separator + backupFileName(pawnExpertFile);
        } else {
            return SettingsService.getSettingsFromThreadName().getStorageArchivePath()
                    + File.separator + dateBackup.getYear()
                    + File.separator + dateBackup.getMonthValue() + "_" + dateBackup.getMonth().toString()
                    + File.separator + pawnExpertFile.getBranchCode()
                    + File.separator + pawnExpertFile.getBranchCode() + " " + pawnExpertFile.getDate().toString() + ".zip";
        }
    }

    /**
     * @param absoluteFileName
     * @return
     */
    private static File createPathToFile(String absoluteFileName) {
        File pathBackUp = new File(absoluteFileName.replaceAll("[\\/][^\\/]+$", ""));
        if (!pathBackUp.exists()) {
            pathBackUp.mkdirs();
        }
        return pathBackUp;
    }

    public static boolean cleanBackup() throws ThreadNotActualException {
        Settings settings = SettingsService.getSettingsFromThreadName();

        File file = new File(settings.getStorageBackupPath());
        Arrays.stream(file.listFiles())
                .filter(File::isDirectory)
                .forEach(o -> Arrays.stream(o.listFiles())
                        .map(BackupFileImpl::new)
                        .sorted(Comparator.comparing(BackupFileImpl::getDate, Comparator.reverseOrder()))
                        .skip(settings.getHowManyBackupsToKeep())
                        .map(e -> e.getFile().toPath())
                        .forEach(y -> {
                            try {
                                if (!Thread.interrupted()) {
                                    Files.delete(y);
                                }
                                //TODO Log file delete
                            } catch (IOException e) {
                                //TODO Log error while deleting file
                            }
                        }));
        return true;
    }


    //TODO Добавить изменение проперти после переименования, добавить на исполнение в какой-то поток.
    public static void renameOldBackups() throws ThreadNotActualException {
        List<File> listFiles = ListFiles.recursive(SettingsService.getSettingsFromThreadName().getStorageBackupPath())
                .collect(Collectors.toList());

        listFiles.addAll(ListFiles.recursive(SettingsService.getSettingsFromThreadName().getStorageArchivePath())
                .collect(Collectors.toList()));

        List<BackupFileImpl> backupsToMove = listFiles.stream().filter(o -> o.getName()
                .matches(BackupFileRegexpEnum.BACKUP_BASEMANAGER_FILE_OLD_REGEXP.getRegexp()))
                .map(BackupFileImpl::new)
                .collect(Collectors.toList());

        for (BackupFileImpl backup : backupsToMove) {
            FileService.moveFile(backup.getFile().toPath(), actualAbsoluteFileName(backup));
        }
    }

    private static Path actualAbsoluteFileName(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        return Paths.get(pawnExpertFile.getFile().getParent()
                + backupFileName(pawnExpertFile));
    }
}
