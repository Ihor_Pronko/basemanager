package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.File;

public class BadFileService {

    public static boolean replaceBadFile(File file) throws ThreadNotActualException {
        Settings settings = SettingsService.getSettingsFromThreadName();
        File absoluteFileName = new File(settings.getBadFilesPath() + File.separator + file.getName());
        //TODO Добавить пометку что файл нужно выгрузить
        return FileService.moveFile(file.toPath(), absoluteFileName.toPath());
    }
}
