package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.file.regexp.BaseFileRegexpEnum;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.file.checkFiles.CheckFile;
import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

public class BaseFileService {

    public BaseFileService() {}

    /**
     * @param file
     * @return
     */
    public static boolean isBaseFile(File file) {
        for (BaseFileRegexpEnum regexp : BaseFileRegexpEnum.values()) {
            if(file.getName().matches(regexp.getRegexp())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param pawnExpertFile
     * @return
     */
    public static boolean replaceBaseFile(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {

        File file = pawnExpertFile.getFile();
        Settings settings = SettingsService.getSettingsFromThreadName();

        CheckFile.moveIfNotRelevance(pawnExpertFile);

        File absoluteFileName = new File(settings.getBasePath() + File.separator + file.getName());
        if (!new File(settings.getBasePath()).exists()) {
            new File(settings.getBasePath()).mkdirs();
        }
        return FileService.moveFile(file.toPath(),
                    absoluteFileName.toPath());
    }

    public static int findBranchCode(File file) throws UndefinedFileException {
        if(isBaseFile(file)) {
            return Integer.valueOf(file.getName().substring(0, 3));
        }
        throw new UndefinedFileException(file);
    }

    public static LocalDate findDateBaseFile(File file) throws UndefinedFileException {
        if(isBaseFile(file)) {
            String date = file.getName().substring(3, 9);
            return LocalDate.of(Integer.valueOf("20" + date.substring(0, 2)),
                    Integer.valueOf(date.substring(2, 4)),
                    Integer.valueOf(date.substring(4, 6)));
        }
        throw new UndefinedFileException(file);
    }
}
