package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.model.file.BuhFileImpl;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.file.regexp.BackupFileRegexpEnum;
import com.ihorp.bmanager.model.file.regexp.BuhFileRegexpEnum;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.file.checkFiles.CheckFile;
import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

public class BuhFileService {

    public BuhFileService() {
    }

    /**
     * @param file
     * @return
     */
    public static boolean isBuhFile(File file) {
        for (BuhFileRegexpEnum regexp : BuhFileRegexpEnum.values()) {
            if(file.getName().matches(regexp.getRegexp())) {
                return true;
            }
        }
        return false;
    }

    public static int findBranchCode(File file) throws UndefinedFileException {
        if(isBuhFile(file)) {
            return Integer.valueOf(file.getName().substring(0, 3));
        }
        throw new UndefinedFileException(file);
    }

    /**
     *
     * @param file
     * @return
     */
    public static LocalDate findDate(File file) throws UndefinedFileException {
        if(isBuhFile(file)) {
            String date = file.getName().substring(3, 9);
            return LocalDate.of(Integer.valueOf("20" + date.substring(0, 2)),
                    Integer.valueOf(date.substring(2, 4)),
                    Integer.valueOf(date.substring(4, 6)));
        }
        throw new UndefinedFileException(file);
    }

    /**
     *
     * @param pawnExpertFile
     * @return
     */
    public static boolean replaceBuhFile(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        File file = pawnExpertFile.getFile();
        Settings settings = SettingsService.getSettingsFromThreadName();

        CheckFile.moveIfNotRelevance(pawnExpertFile);

        File absoluteFileName = new File(settings.getBuhPath() + File.separator + file.getName());
        
        if (!new File(settings.getBuhPath()).exists()) {
            new File(settings.getBuhPath()).mkdirs();
        }
        return FileService.moveFile(file.toPath(),
                absoluteFileName.toPath());
    }
}

