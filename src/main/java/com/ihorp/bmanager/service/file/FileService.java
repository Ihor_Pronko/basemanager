package com.ihorp.bmanager.service.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import java.nio.file.StandardCopyOption;

public class FileService {

    /**
     * TODO
     * @param srcFile
     * @param dstFile
     * @return
     */
    public static boolean moveFile(Path srcFile, Path dstFile) {
        mkDirsToFile(dstFile);
        try {
            Files.move(srcFile,
                    dstFile,
                    StandardCopyOption.REPLACE_EXISTING);
           return true;
        } catch (IOException e) {
            // TODO Log
            e.printStackTrace();// Add log
        }
        return false;
    }

    public static boolean copyFile(Path srcFile, Path dstFile) {
        mkDirsToFile(dstFile);
        try {
            Files.copy(srcFile,
                    dstFile,
                    StandardCopyOption.REPLACE_EXISTING);
            return true;
        } catch (IOException e) {
            //TODO
            e.printStackTrace();
        }
        return false;
    }

    private static void mkDirsToFile(Path file) {
        File pathToFile = file.getParent().toFile();
        if (!pathToFile.exists()) {
            pathToFile.mkdirs();
        }
    }


}
