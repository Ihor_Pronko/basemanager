package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.model.file.BackupFileImpl;
import com.ihorp.bmanager.model.file.BadFile;
import com.ihorp.bmanager.model.file.BaseFileImpl;
import com.ihorp.bmanager.model.file.BuhFileImpl;
import com.ihorp.bmanager.model.file.NonExpectedFile;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.file.UpdateLogFileImpl;
import com.ihorp.bmanager.service.file.checkFiles.CheckFile;

import java.io.File;

/**
 *
 */
public class IdentifyFile {

    public static PawnExpertFile identifyFile(File file) {

        if (file == null) {
            return null;
        }

        //TODO Навести красоту
        try {
            if (file.getName().matches(".*\\.bad$|.*\\.bed$") || CheckFile.isBad(file)) {
                return new BadFile(file);
            } else if (BackupFileService.isBackup(file)) {
                return new BackupFileImpl(file);
            } else if (BuhFileService.isBuhFile(file)) {
                return new BuhFileImpl(file);
            } else if (BaseFileService.isBaseFile(file)) {
                return new BaseFileImpl(file);
            } else if (UpdateLogFileService.isUpdateFile(file)) {
                return new UpdateLogFileImpl(file);
            } else if (PawnExpetFileService.isPawnExpertZipFile(file)) {
                return identifyFile(UnzipFile.unzip(file));
            } else if (!file.getName().matches(".*\\.tmp$")) {
                return new NonExpectedFile(file);
            }
        } catch (UndefinedFileException e) {
            return new NonExpectedFile(file);
        }
        return null;
    }
}
