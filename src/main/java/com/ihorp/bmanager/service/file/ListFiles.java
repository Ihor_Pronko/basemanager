package com.ihorp.bmanager.service.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListFiles {

    public static List<File>  inCurrentPath(String path) {
        File file = new File(Paths.get(path).toAbsolutePath().toString());
        if (file.exists() && file.isDirectory() && file.listFiles().length > 0) {
            return Arrays.stream(file.listFiles()).filter(File::isFile).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public static Stream<File> recursive(String path) {
        try {
            return Files.walk(Paths.get(path)).map(Path::toFile).filter(o -> o.isFile());
        } catch (IOException e) {
            //TODO to log
            e.printStackTrace();
        }
        return Stream.empty();
    }
}
