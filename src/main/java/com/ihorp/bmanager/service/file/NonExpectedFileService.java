package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class NonExpectedFileService {

    /**
     *
     * @param pawnExpertFile
     * @return
     */
    public static boolean replace(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        File file = pawnExpertFile.getFile();
        Settings settings = SettingsService.getSettingsFromThreadName();
        File absoluteFileName = new File(settings.getUfoFilesPath() + File.separator + file.getName());
        if (!new File(settings.getUfoFilesPath()).exists()) {
            new File(settings.getUfoFilesPath()).mkdirs();
        }
        return FileService.moveFile(file.toPath(),
                absoluteFileName.toPath());
    }
}
