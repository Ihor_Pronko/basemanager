package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.file.regexp.BackupFileRegexpEnum;
import com.ihorp.bmanager.model.file.regexp.BaseFileRegexpEnum;
import com.ihorp.bmanager.model.file.regexp.BuhFileRegexpEnum;
import com.ihorp.bmanager.model.file.regexp.UpdateFileRegexpEnum;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PawnExpetFileService {

    /**
     * Chech is filename starts with PE file.
     * @param file
     * @return
     */
    public static boolean isPawnExpertZipFile(File file) {

        List<String> values = new ArrayList<>();

        values.add(BackupFileRegexpEnum.BACKUP_PAWNEXPERT_FILE_REGEXP.getZipRegexp());

        values.add(BackupFileRegexpEnum.BACKUP_PAWNEXPERT_FILE_SHOP_REGEXP.getZipRegexp());

        values.addAll(Arrays.stream(BaseFileRegexpEnum.values())
                .map(BaseFileRegexpEnum::getZipRegexp).collect(Collectors.toList()));
        values.addAll(Arrays.stream(BuhFileRegexpEnum.values())
                .map(BuhFileRegexpEnum::getZipRegexp).collect(Collectors.toList()));
        values.addAll(Arrays.stream(UpdateFileRegexpEnum.values())
                .map(UpdateFileRegexpEnum::getZipRegexp).collect(Collectors.toList()));

        return values.stream().anyMatch(o -> file.getName().matches(o));
    }
}
