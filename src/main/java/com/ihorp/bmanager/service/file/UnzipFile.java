package com.ihorp.bmanager.service.file;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;


public class UnzipFile {

    /**
     *
     * @param file
     * @return
     */
    public static File unzip(File file) {

        if (!file.exists() || !file.canRead()) {
            System.out.println("File " + file.getName() + " cannot be read");
            return null;
        }

        File filePE;
        try {
            ZipFile zip = new ZipFile(file);
            Enumeration entries = zip.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();

                if (entry.isDirectory()) {
                    new File(file.getParent(), entry.getName()).mkdirs();
                } else {
                    write(zip.getInputStream(entry),
                            new BufferedOutputStream(new FileOutputStream(
                                    new File(file.getParent(), entry.getName()))));
                }
            }
            zip.close();
            Files.delete(file.toPath());
            //TODO Log

            filePE = new File(file.getParent(), file.getName().replaceAll(".zip$", ""));

        } catch (ZipException p) {
            //TODO Log
            try {
                Files.move(file.toPath(), Paths.get(file.getAbsolutePath() + ".bad"), StandardCopyOption.REPLACE_EXISTING);
                //TODO Log
                return new File(file.getAbsolutePath() + ".bad");
            } catch (IOException e) {
                //TODO Log
            }
            return null;
        } catch (IOException e) {
            //TODO Log
            return null;
        }
        return filePE;
    }

    /**
     *
     * @param in
     * @param out
     * @throws IOException
     */
    private static void write(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) >= 0)
            out.write(buffer, 0, len);
        out.close();
        in.close();
    }
}
