package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.service.dao.googleSheets.BranchKeeper;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.file.regexp.UpdateFileRegexpEnum;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.branch.BranchService;
import com.ihorp.bmanager.service.dao.googleSheets.GoogleSheetsDAO;
import com.ihorp.bmanager.service.file.checkFiles.CheckFile;
import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.File;
import java.security.URIParameter;
import java.time.LocalDate;

public class UpdateLogFileService {

    /**
     * @param file
     * @return
     */
    public static boolean isUpdateFile(File file) {
        for (UpdateFileRegexpEnum regexp : UpdateFileRegexpEnum.values()) {
            if(file.getName().matches(regexp.getRegexp())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param updateFile
     * @return
     */
    public static boolean replace(PawnExpertFile updateFile) throws ThreadNotActualException {

        CheckFile.moveIfNotRelevance(updateFile);

        Settings settings = SettingsService.getSettingsFromThreadName();

        File absoluteFile = getAbsoluteFile(updateFile, settings);

        if (!new File(settings.getUpdateLogPath()).exists()) {
            new File(settings.getUpdateLogPath()).mkdirs();
        }
        return FileService.moveFile(updateFile.getFile().toPath(),
                absoluteFile.toPath());
    }

    /**
     *
     * @param updateFile
     * @return
     */
    private static File getAbsoluteFile(PawnExpertFile updateFile, Settings settings) throws ThreadNotActualException {
        GoogleSheetsDAO googleSheetsDAO = GoogleSheetsDAO.getInstance();
        if (googleSheetsDAO.matchCompany() && googleSheetsDAO.getBranch(updateFile).getCodeOfBranch() != 0) {
            String fullBranchName = googleSheetsDAO.getBranch(updateFile).getFullBranchName();
            return new File(settings.getUpdateLogPath()
                    + File.separator + fullBranchName
                    + File.separator + "Update " + fullBranchName + " " + updateFile.getDate() + ".txt");
        } else {
            return new File(settings.getUpdateLogPath()
                    + File.separator + updateFile.getBranchCode()
                    + File.separator + "Update " + updateFile.getBranchCode() + " "
                    + updateFile.getDate() + ".txt");
        }
    }
    /**
     *
     * @param file
     * @return
     */
    //TODO Протестировать
    public static int findBranchCode(File file) {
        if(file.getName().matches(UpdateFileRegexpEnum.UPDATE_PAWNEXPERT_FILE_REGEXP.getRegexp())) {
            return Integer.valueOf(file.getName().replaceAll(".*\\.", ""));
        } else if (file.getName().matches(UpdateFileRegexpEnum.UPDATE_BASEMANAGER_FILE_REGEXP.getRegexp())) {
            return Integer.parseInt(file.getName().replaceAll("Update ", "").replaceAll("//s.*", ""));
        } else if (file.getName().matches(UpdateFileRegexpEnum.UPDATE_BASEMANAGER_FILE_NEW_REGEXP.getRegexp())) {
            return Integer.parseInt(file.getName().replaceAll(".*\\(", "").replaceAll("\\).*", ""));
        } else {
            return 0;
        }
    }

    /**
     *
     * @param file
     * @return
     */
    //TODO Протестировать.
    public static LocalDate findDate(File file) throws UndefinedFileException {
        String date;
        if(file.getName().matches(UpdateFileRegexpEnum.UPDATE_PAWNEXPERT_FILE_REGEXP.getRegexp())) {
            date = file.getName().replaceAll(".*_", "");
            return LocalDate.of(Integer.valueOf("20" + date.substring(0, 2)),
                    Integer.valueOf(date.substring(2, 4)),
                    Integer.valueOf(date.substring(4, 6)));
        } else if (file.getName().matches(UpdateFileRegexpEnum.UPDATE_BASEMANAGER_FILE_REGEXP.getRegexp()) ||
                file.getName().matches(UpdateFileRegexpEnum.UPDATE_BASEMANAGER_FILE_NEW_REGEXP.getRegexp())) {
            date = file.getName().replaceAll(".*\\s", "");
            return LocalDate.of(Integer.valueOf(date.substring(0, 4)),
                    Integer.valueOf(date.substring(5, 7)),
                    Integer.valueOf(date.substring(8, 10)));
        } else {
            return LocalDate.of(1990, 01, 01);
        }
    }
}
