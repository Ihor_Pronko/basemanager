package com.ihorp.bmanager.service.file.checkFiles;

import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.model.file.BuhFileImpl;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.service.file.BuhFileService;
import com.ihorp.bmanager.service.file.FileService;
import com.ihorp.bmanager.service.file.ListFiles;

import java.io.File;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CheckBuhFileService {

    /**
     *
     */
    public static List<BuhFileImpl> checkBuhFiles(Settings settings) {
        LocalDate startDate = LocalDate.now().minusMonths(3);
        List<BuhFileImpl> buhFiles = getListBuhFiles(settings.getBuhPath(), startDate);
        List<BuhFileImpl> missedFiles = new ArrayList<>();

        for (int i = 0; i < buhFiles.size() - 1; i++) {
            //CheckFile.isBad очень сильно тормозит при первом проходе (1000 итераций (Файлов) - 12-14 секунд на сервере BaseExpert)
            if (CheckFile.isBad(buhFiles.get(i).getFile())) {
                FileService.moveFile(buhFiles.get(i).getFile().toPath(),
                        Paths.get(settings.getBadFilesPath() + File.separator + buhFiles.get(i).getFile().getName()));
                //TODO Log buhFiles.get(i).getFile().getName() is bad
                missedFiles.add(buhFiles.get(i));
            }

            BuhFileImpl currentBuhFile = null;
            try {
                currentBuhFile = new BuhFileImpl(buhFiles.get(i).getFile());
            } catch (UndefinedFileException e) {
                //TODO Do nothing.
            }
            if (buhFiles.get(i).getBranchCode() != buhFiles.get(i + 1).getBranchCode()) {
                continue;
            }

            BuhFileImpl nextBuhFile;
            boolean bool = true;
            while(bool) {
                nextBuhFile = nextFile(currentBuhFile);
                if (!buhFiles.get(i + 1).equals(nextBuhFile)) {
                    missedFiles.add(nextBuhFile);
                    currentBuhFile = nextBuhFile;
                } else {
                    bool = false;
                }
            }
        }
        return missedFiles;
    }


    private static List<BuhFileImpl> getListBuhFiles(String buhPath, LocalDate startDate) {
        return ListFiles.inCurrentPath(buhPath).stream()
                .filter(BuhFileService::isBuhFile)
                .sorted(Comparator.comparing(File::getName))
                .map(o -> {
                    try {
                        return new BuhFileImpl(o);
                    } catch (UndefinedFileException e) {
                        //TODO Do nothing
                        return null;
                    }
                })
                .filter(o -> o.getDate().isAfter(startDate))
                .collect(Collectors.toList());
    }

    private static BuhFileImpl nextFile(BuhFileImpl currentBuhFile) {
        LocalDate nextDay = currentBuhFile.getDate().plusDays(1);
        boolean endOfMonth = currentBuhFile.getDate().getDayOfMonth() > nextDay.getDayOfMonth();

        try {
            if (currentBuhFile.getFile().getName().matches(".*\\.002$")) {
                return new BuhFileImpl(new File(currentBuhFile.getFile().getAbsolutePath().replaceAll("\\.002$", ".003")));
            } else if (currentBuhFile.getFile().getName().matches(".*\\.003$") && endOfMonth) {
                return new BuhFileImpl(new File(currentBuhFile.getFile().getAbsolutePath().replaceAll("\\.003$", ".004")));
            } else if (currentBuhFile.getFile().getName().matches(".*\\.003$")) {
                return new BuhFileImpl(new File(currentBuhFile.getFile().getAbsolutePath().replaceAll("\\.003$", ".502")));
            } else if (currentBuhFile.getFile().getName().matches(".*\\.004$")) {
                return new BuhFileImpl(new File(currentBuhFile.getFile().getAbsolutePath().replaceAll("\\.004$", ".502")));
            } else {
                return new BuhFileImpl(new File(currentBuhFile.getFile().getAbsolutePath().replaceAll("\\d{6}\\.502$",
                        nextDay.toString().replaceAll("^20", "").replaceAll("-", "")
                                + ".002")));
            }
        } catch (UndefinedFileException e) {
            //TODO Do nothing
            return null;
        }
    }


}
