package com.ihorp.bmanager.service.file.checkFiles;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.file.BadFile;
import com.ihorp.bmanager.model.file.NonExpectedFile;
import com.ihorp.bmanager.model.file.PawnExpertFile;
import com.ihorp.bmanager.model.file.regexp.BaseFileRegexpEnum;
import com.ihorp.bmanager.service.dao.googleSheets.GoogleSheetsDAO;
import com.ihorp.bmanager.service.file.BaseFileService;
import com.ihorp.bmanager.service.file.BuhFileService;
import com.ihorp.bmanager.service.file.FileService;
import com.ihorp.bmanager.service.settings.SettingsService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 */
public class CheckFile {

    /**
     *
     * @param file
     * @return
     */
    public static boolean isBad(File file) {

        if (BuhFileService.isBuhFile(file)) {
            return isBuhFileBad(file);
        } else if (BaseFileService.isBaseFile(file)) {
            return isBaseFileBad(file);
        }
        return false;
    }

    /**
     *
     * @param file
     * @return
     */
    private static boolean isBuhFileBad(File file) {
        if (file == null || !file.exists()) {
            return false;
        } else if (BuhFileService.isBuhFile(file)) {
            return isBadByRegular(file, ".*1a$|.*0d$");
        } else {
            return false;
        }
    }

    private static boolean isBaseFileBad(File file) {
        if (file.getName().matches(BaseFileRegexpEnum.BASE_PAWNEXPERT_FILE_100_REGEXP.getRegexp())) {
            return isBadByRegular(file, ".*07$");
        } else {
            return false;
        }
    }

    /**
     *
     * @param file
     * @param regex
     * @return
     */
    private static boolean isBadByRegular(File file, String regex) {

        int read;
        StringBuilder hex = new StringBuilder();
        String valueRead;

        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
            while ((read = in.read()) != -1) {
                valueRead = Integer.toHexString(read);
                if (valueRead.length() == 1) {
                    hex = hex.append("0");
                }
                hex = hex.append(valueRead);
            }
            return !hex.toString().matches(regex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean moveIfNotRelevance(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        if(!isFileRelevance(pawnExpertFile)) {
            moveToNotRelevance(pawnExpertFile);
            //TODO ALARM!!! File pawnExpertFile is not relevance. Add notification.
            return false;
        }
        return true;
    }

    private static boolean isFileRelevance(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        if (pawnExpertFile instanceof NonExpectedFile
                || pawnExpertFile instanceof BadFile) {
            return true;
        }
        return GoogleSheetsDAO.getInstance().getBranch(pawnExpertFile) != null;
    }

    private static void moveToNotRelevance(PawnExpertFile pawnExpertFile) throws ThreadNotActualException {
        FileService.moveFile(pawnExpertFile.getFile().toPath(),
                new File(SettingsService.getSettingsFromThreadName().getNotRelevanceFilePath()
                        + File.separator + pawnExpertFile.getFile().getName()).toPath());
    }
}
