package com.ihorp.bmanager.service.settings;

import com.ihorp.bmanager.model.settings.settingsEnum.SettingsField;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;

/**
 * PropertyService class.
 */
public final class PropertyService {

    /**
     * Constructor of PropertyService class.
     */
    private PropertyService() {
    }

    /**
     * Get property from property file.
     * @param file property file.
     * @param field field which need to get.
     * @return property.
     */
    public static String getProperty(File file, SettingsField field) {

        PropertiesConfiguration config;
        try {
            config = new PropertiesConfiguration(file.getName());
        } catch (ConfigurationException e) {
            return "";
        }
        return config.getString(field.getParameter());
    }

    /**
     * Write property to file.
     * @param file property file.
     * @param settingsField field which need to write.
     * @param string property which need to write.
     * @return true or false.
     */
    public static boolean writeProperty(File file, SettingsField settingsField, String string) {

        PropertiesConfiguration configuration;
        try {
            configuration = new PropertiesConfiguration(file);
            configuration.setProperty(settingsField.getParameter(), string);
            configuration.save();
        } catch (ConfigurationException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
