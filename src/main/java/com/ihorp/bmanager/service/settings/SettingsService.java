package com.ihorp.bmanager.service.settings;

import com.ihorp.bmanager.model.exception.ThreadNotActualException;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsField;
import com.ihorp.bmanager.model.settings.SettingsKeeper;
import com.ihorp.bmanager.service.thread.ThreadService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A service class.
 */
public final class SettingsService {

    /**
     * Default constructor.
     */
    private SettingsService() {
    }

    /**
     * Create a Example Settings file in root path.
     *
     * @return true or false.
     */
    public static boolean createExampleSettings(File file) {

        if (!file.exists()) {
            try {
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(SettingsDefault.COMMENT_IN_SETTINGS_FILE.getStringValue());
                fileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        for (SettingsField value : SettingsField.values()) {
            PropertyService.writeProperty(file, value, value.getExampleProperty());
        }
        return true;
    }

    /**
     * Get name of company from properties file.
     *
     * @param file file to parse.
     * @return company name.
     */
    public static String getCompanyName(File file) {
        String companyName = PropertyService.getProperty(file, SettingsField.COMPANY_NAME);
        if (companyName == null || companyName.equals("")) {
            PropertyService.writeProperty(file, SettingsField.ACTIVE_SETTINGS,
                    "0, Write a company name!");
            return null;
        }
        return companyName;
    }

    /**
     * Is properties file active.
     *
     * @param file file to parse.
     * @return is settings active.
     */
    public static boolean getIsActive(File file) {
        String isActive = PropertyService.getProperty(file, SettingsField.ACTIVE_SETTINGS);
        if (isActive == null) {
            PropertyService.writeProperty(file, SettingsField.ACTIVE_SETTINGS,
                    "0");
            return false;
        }
        return isActive.equals("1") || isActive.equals("true");
    }

    /**
     * Get ftp path from properties file.
     *
     * @param file file to parse.
     * @return ftp path.
     */
    public static Path getFtpPath(File file) {
        String path = PropertyService.getProperty(file, SettingsField.FTP_PATH);
        if (path == null || path.equals("") || !(path.matches(SettingsDefault.PATH_PATTERN_WINDOWS.getStringValue())
                || path.matches(SettingsDefault.PATH_PATTERN_UNIX.getStringValue()))) {
            PropertyService.writeProperty(file, SettingsField.ACTIVE_SETTINGS,
                    SettingsDefault.FTP_PATH_ERROR_MESSAGE.getStringValue());
            return null;
        }
        if (path.matches("^.*[\\\\/]$")) {
            path =  path + PropertyService.getProperty(file, SettingsField.COMPANY_NAME);
        } else {
            path = path + File.separator + PropertyService.getProperty(file, SettingsField.COMPANY_NAME);
        }
        return new File(path).toPath();
    }

    /**
     * Get storage path from properties file.
     *
     * @param file file to parse.
     * @return storage path.
     */
    public static Path getStoragePath(File file) {
        String path = PropertyService.getProperty(file, SettingsField.STORAGE_PATH);
        if (path == null || path.equals("") || !(path.matches(SettingsDefault.PATH_PATTERN_WINDOWS.getStringValue())
                || path.matches(SettingsDefault.PATH_PATTERN_UNIX.getStringValue()))) {
            PropertyService.writeProperty(file, SettingsField.ACTIVE_SETTINGS,
                    SettingsDefault.STORAGE_PATH_ERROR_MESSAGE.getStringValue());
            return null;
        }
        if (path.matches(".*[\\\\/]$")) {
            path =  path + PropertyService.getProperty(file, SettingsField.COMPANY_NAME);
        } else {
            path = path + File.separator + PropertyService.getProperty(file, SettingsField.COMPANY_NAME);
        }
        return new File(path).toPath();
    }

    /**
     * Get integer how many backups to keep from properties file.
     *
     * @param file file to parse.
     * @return hw many backups to keep.
     */
    public static int getHowManyBackupsToKeep(File file) {
        String property = PropertyService.getProperty(file, SettingsField.HOW_MANY_BACKUPS_TO_KEEP);
        if (property == null|| property.equals("") || !property.matches("^[1-9]+.*$")) {

            PropertyService.writeProperty(file, SettingsField.HOW_MANY_BACKUPS_TO_KEEP,
                    "The default value is set: "
                            + SettingsDefault.HOW_MANY_BACKUPS_TO_KEEP.getIntegerValue());

            return SettingsDefault.HOW_MANY_BACKUPS_TO_KEEP.getIntegerValue();
        }
        Matcher matcher = Pattern.compile("[1-9][0-9]*").matcher(property);
        matcher.find();
        return Integer.parseInt(matcher.group());
    }

    /**
     * Get min date to archive from properties file.
     *
     * @param file file to parse.
     * @return min date to archive.
     */
    public static int getMinDateToArchive(File file) {
        String property = PropertyService.getProperty(file, SettingsField.MIN_DATE_TO_ARCHIVE);
        if (property == null || property.equals("") || !property.matches("^[1-9]+.*")) {
            PropertyService.writeProperty(file, SettingsField.MIN_DATE_TO_ARCHIVE,
                    "The default value is set: "
                            + SettingsDefault.MIN_DATE_TO_ARCHIVE.getIntegerValue());
            return SettingsDefault.MIN_DATE_TO_ARCHIVE.getIntegerValue();
        }
        Matcher matcher = Pattern.compile("[1-9][0-9]*").matcher(property);
        matcher.find();
        return Integer.parseInt(matcher.group());
    }

    /**
     * Get path with actual backups from properties file.
     *
     * @param file file to parse.
     * @return path with actual backups files.
     */
    public static String getStorageBackupPath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.BACKUP_PATH_NAME.getStringValue();
    }

    /**
     * Get path with archive backups from properties file.
     *
     * @param file file to parse.
     * @return path with archive files.
     */
    public static String getStorageArchivePath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.BACKUP_ARCHIVE_PATH_NAME.getStringValue();
    }

    /**
     * Get Buh path from properties file.
     *
     * @param file file to parse.
     * @return path with buh files.
     */
    public static String getBuhPath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.BUH_PATH.getStringValue() + getCompanyName(file);
    }

    /**
     * Get base path from properties file.
     *
     * @param file file to parse.
     * @return path with base files.
     */
    public static String getBasePath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.BASE_PATH.getStringValue();
    }

    /**
     * Get update path from properties file.
     *
     * @param file file to parse.
     * @return path with update log files.
     */
    public static String getUpdateLogPath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.UPDATE_LOG_PATH.getStringValue();
    }

    /**
     * Get path for ufo files from properties file.
     *
     * @param file file to parse.
     * @return path with non recognized files.
     */
    public static String getUfoFilesPath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.UFO_FILES_PATH.getStringValue();
    }

    /**
     * Get bad path from properties file.
     *
     * @param file file to parse.
     * @return path with bad files.
     */
    public static String getBadFilesPath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.BAD_FILES_PATH.getStringValue();
    }

    /**
     * Get spread sheet id from properties file.
     *
     * @param file file to parse.
     * @return sheet id.
     */
    public static String getSpreadSheetId(File file) {
        String property = PropertyService.getProperty(file, SettingsField.ID_GOOGLE_SHEET);
        if(property != null) {
            return property;
        }
        return "";
    }

    /**
     * Get not relevance path from properties file.
     * @param file file to parse.
     * @return path with not relevance files.
     */
    public static String getNotRelevanceFilePath(File file) {
        return getStoragePath(file) + File.separator
                + SettingsDefault.NOT_RELEVANCE_PATH.getStringValue();
    }

    public static void duplicationSettings(Settings settings) {
        PropertyService.writeProperty(settings.getFile(), SettingsField.ACTIVE_SETTINGS,
                "0, DUPLICATION FOUND!!!");
        settings.setNonActive();
    }

    public static Settings getSettingsFromThreadName() throws ThreadNotActualException {
        if(SettingsKeeper.getInstance().matchActiveCompany(ThreadService.getCompanyName())) {
            return SettingsKeeper.getInstance().getActiveCompanySettings(ThreadService.getCompanyName());
        }
        //TODO Log Thread Stopped.
        throw new ThreadNotActualException();
    }
}
