package com.ihorp.bmanager.service.settings.updates;

import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.model.settings.SettingsKeeper;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.model.thread.GoogleSheetsThread;
import com.ihorp.bmanager.model.thread.SettingsThread;
import com.ihorp.bmanager.service.file.ListFiles;
import com.ihorp.bmanager.service.settings.SettingsService;
import com.ihorp.bmanager.service.thread.ThreadStarter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SettingsUpdates {

    /**
     *
     */
    public synchronized void checkSettingsUpdates() {

        List<Settings> fileSettingsList = ListFiles.inCurrentPath("").stream()
                .filter(o -> o.getName().matches("^Settings_.*\\.properties$"))
                .filter(o -> !o.getName().matches(SettingsDefault.DEFAULT_SETTINGS_FILE_NAME.getStringValue()))
                .map(Settings::new).collect(Collectors.toList());

        deleteIfFileNotExists(fileSettingsList);
        addNewSettingsToCollection(fileSettingsList);
    }

    /**
     * Find the settings in collection which do not actual.
     */
    private boolean deleteIfFileNotExists(List<Settings> fileSettingsList) {
        List<Settings> listToDelete = SettingsKeeper.getInstance().getSettings()
                .entrySet().stream().map(Map.Entry::getValue)
                .filter(c -> fileSettingsList.stream().allMatch(f -> f.hashCode() != c.hashCode()))
                .collect(Collectors.toList());
        if (!listToDelete.isEmpty()) {
            deleteSettings(listToDelete);
            return true;
        }
        return false;
    }

    /**
     * Delete settings from collection.
     *
     * @param settings instance of settings
     * @return boolean value.
     */
    private void deleteSettings(List<Settings> settings) {
        settings.forEach(o -> {
            SettingsKeeper.getInstance().deleteCompanySettings(o);
            System.out.println(Thread.currentThread().getName() + ": Settings " + o.getCompanyName() + " deleted :" + LocalDateTime.now());
        });
    }

    /**
     *
     */
    private boolean addNewSettingsToCollection(List<Settings> fileSettingsList) {
        List<Settings> newSettings = fileSettingsList.stream()
                .filter(f -> !SettingsKeeper.getInstance().matchCompany(f.hashCode()))
                .collect(Collectors.toList());

        newSettings.forEach(f -> {
            if (f.isActive() && SettingsKeeper.getInstance().getSettings().entrySet().stream()
                    .map(Map.Entry::getValue)
                    .filter(Settings::isActive)
                    .anyMatch(c -> c.getCompanyName().equals(f.getCompanyName()))) {
                SettingsService.duplicationSettings(f);
            }
            addSettings(f);
        });
        return !newSettings.isEmpty();
    }

    /**
     * Add a new settings in collection. If this actual settings - run new thread.
     *
     * @param settings
     */
    private void addSettings(Settings settings) {
        SettingsKeeper.getInstance().addCompanySettings(settings);
        if (settings.isActive()) {
            ThreadStarter threadStarter = new ThreadStarter();
            threadStarter.runAllFileThreads(settings);
        }
    }
}
