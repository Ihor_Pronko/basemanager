package com.ihorp.bmanager.service.sheets;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsField;
import com.ihorp.bmanager.service.settings.PropertyService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class SheetServices {


    /**
     * Build and return an authorized Sheets API client service.
     * @return an authorized Sheets API client service
     * @throws IOException
     */
    public static List<List<Object>> getSheet(Settings settings, String range) {
        // Build a new authorized API client service.
        Credential credential = GoogleAutorize.authorize();
        Sheets service = new Sheets.Builder(GoogleAutorize.HTTP_TRANSPORT, GoogleAutorize.JSON_FACTORY, credential)
                .setApplicationName(GoogleAutorize.APPLICATION_NAME)
                .build();

        // Prints the names and majors of students in a sample spreadsheet:

        ValueRange response = null;
        try {
            response = service.spreadsheets().values()
                    .get(settings.getSpreadSheetId(), range)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
            PropertyService.writeProperty(settings.getFile(), SettingsField.ID_GOOGLE_SHEET,
                    SettingsDefault.GOOGLE_SHEETS_ID_ERROR_MESSAGE.getStringValue() + PropertyService.getProperty(settings.getFile(), SettingsField.ID_GOOGLE_SHEET));
        }
        List<List<Object>> values = new ArrayList<>();
        if (response != null) {
            values = response.getValues();
        }
        return values;
    }
}
