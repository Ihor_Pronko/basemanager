package com.ihorp.bmanager.service.thread;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class ThreadService {

    /**
     *
     * @param sec
     */
    public static void sleep(int sec) {
        try {
            TimeUnit.SECONDS.sleep(sec);
        } catch (InterruptedException e) {
            e.printStackTrace();
            //TODO add Logger.
        }
    }

    /**
     *
     * @param monitor
     */
    public static void waitThread(String monitor) {
        synchronized (monitor) {
            try {
                System.out.println(Thread.currentThread().getName() + " : Wait for : " + monitor + " thread. :" + LocalDateTime.now());
                monitor.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param monitor
     */
    public static void notifyAllThreads(String monitor) {
        synchronized (monitor) {
            monitor.notifyAll();
        }
    }

    /**
     *
     * @return
     */
    //TODO Поменять на переменную в классе потока!!!
    public static String getCompanyName() {
        return Thread.currentThread().getName().replaceAll(":.*$", "");
    }
}
