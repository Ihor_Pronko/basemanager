package com.ihorp.bmanager.service.thread;

import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.model.thread.GoogleSheetsThread;
import com.ihorp.bmanager.model.thread.SettingsThread;
import com.ihorp.bmanager.model.thread.fileThread.CheckBuhFilesThread;
import com.ihorp.bmanager.model.thread.fileThread.CleanFilesThread;
import com.ihorp.bmanager.model.thread.fileThread.MoveFilesThread;

public class ThreadStarter extends Thread {

    /**
     *
     */
    private String settingsMonitor = "settings monitor";

    /**
     *
     */
    private String sheetsMonitor = "sheets monitor";

    /**
     *
     */
    private String moveFileMonitor = "move files monitor";

    /**
     *
     */
    public void runSettingsThread() {
        SettingsThread settingsThread = new SettingsThread(settingsMonitor);
        settingsThread.start();
    }

    /**
     *
     */
    public void runGoogleSheetsThread() {
        GoogleSheetsThread googleSheetsThread = new GoogleSheetsThread(settingsMonitor, sheetsMonitor);
        googleSheetsThread.start();
    }

    /**
     *  Запускает файловые потоки (Перемещение, очистка старых файлов, проверка актуальности файлов). Называет поток
     *  по имени компании по которой работает.
     * @param settings
     */
    //TODO название компании можно хранить в переменной Thread.
    public void runAllFileThreads(Settings settings) {
        CleanFilesThread cleanFilesThread = new CleanFilesThread(moveFileMonitor);
        new Thread(cleanFilesThread, settings.getCompanyName() + ":CleanFilesThread").start();

        MoveFilesThread moveFilesThread = new MoveFilesThread(sheetsMonitor, moveFileMonitor);
        new Thread(moveFilesThread, settings.getCompanyName() + ":MoveFilesThread").start();

        CheckBuhFilesThread checkBuhFiles = new CheckBuhFilesThread(moveFileMonitor);
        new Thread(checkBuhFiles, settings.getCompanyName()+ ":CheckBuhFilesThread").start();
    }
}
