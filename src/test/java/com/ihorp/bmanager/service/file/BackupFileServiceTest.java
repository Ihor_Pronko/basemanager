package com.ihorp.bmanager.service.file;

import com.ihorp.bmanager.model.file.BackupFileImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class BackupFileServiceTest {


    private File peBackup;
    private File peBackupWithShop;
    private File bmBackup;
    private File bmBackupNew;

    @Before
    public void before() {
        peBackup = new File("BackUp_1_000932_161203_195642.bak");
        peBackupWithShop = new File("BackUp_11_000001_170615_121253.bak");
        bmBackup = new File("5 2017-04-04.zip");
        bmBackupNew = new File("Backup №4 (4) Тарутине 2017-06-15.zip");
    }

    @Test
    public void isBackupTest() {
        Assert.assertTrue(BackupFileService.isBackup(peBackup));
    }

    @Test
    public void isBackupTestTwo() {
        Assert.assertTrue(BackupFileService.isBackup(peBackupWithShop));
    }

    @Test
    public void isBackupTestThree() {
        Assert.assertTrue(BackupFileService.isBackup(bmBackup));
    }

    @Test
    public void isBackupTestFour() {
        Assert.assertTrue(BackupFileService.isBackup(bmBackupNew));
    }

    @Test
    public void findBranchCodeTest() {
        Assert.assertTrue(BackupFileService.findBranchCode(peBackup) == 932);
    }

    @Test
    public void findBranchCodeTestTwo() {
        Assert.assertTrue(BackupFileService.findBranchCode(peBackupWithShop) == 1);
    }

    @Test
    public void findBranchCodeTestThree() {
        Assert.assertTrue(BackupFileService.findBranchCode(bmBackup) == 5);
    }

    @Test
    public void findBranchCodeTestFour() {
        Assert.assertTrue(BackupFileService.findBranchCode(bmBackupNew) == 4);
    }
}