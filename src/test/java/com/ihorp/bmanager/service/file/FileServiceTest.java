package com.ihorp.bmanager.service.file;

import org.junit.Test;

import java.io.File;
import java.nio.file.Path;

public class FileServiceTest {

    @Test
    public void moveFileTest() {
        Path pathSrc = new File("/Users/ihor/Desktop/test/ftp/Komod/In/up0021_170111.932").toPath();
        Path pathDst = new File("/Users/ihor/Desktop/test/ftp/Komod/Out/Test/up0021_170111.932").toPath();
        FileService.moveFile(pathSrc, pathDst);
    }
}
