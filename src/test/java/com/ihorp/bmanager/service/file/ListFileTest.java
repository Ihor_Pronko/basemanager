package com.ihorp.bmanager.service.file;

import com.google.common.io.PatternFilenameFilter;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

public class ListFileTest {

    @Test
    public void testOne() {
        PatternFilenameFilter patternFilenameFilter = new PatternFilenameFilter("^Settings_.*\\.properties$");

        File file = new File("");
        Arrays.stream(file.listFiles(patternFilenameFilter)).forEach(System.out::println);
    }
}
