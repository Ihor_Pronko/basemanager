package com.ihorp.bmanager.service.settings;

import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.service.CreateStopFile;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;

import static org.junit.Assert.assertTrue;

public class CreateStopFileTest {

    private static File fileName = new File(SettingsDefault.STOP_PROGRAM_FILE_NAME.getStringValue());

    @BeforeClass
    public static void before() {
        fileName.delete();
    }

    @Test
    public void createTest() {
        Assert.assertEquals(CreateStopFile.create(fileName), true);
    }

    @Test
    public void createTestTwo() {
        Assert.assertEquals(CreateStopFile.create(new File("/NonExpectedPath/NonExpectedFile.txt")), false);
    }

    @Test
    public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<CreateStopFile> constructor = CreateStopFile.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @AfterClass
    public static void after() {
        try {
            Files.deleteIfExists(fileName.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
