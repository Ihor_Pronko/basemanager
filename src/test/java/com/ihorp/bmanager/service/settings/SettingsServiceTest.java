package com.ihorp.bmanager.service.settings;

import com.ihorp.bmanager.model.settings.Settings;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import com.ihorp.bmanager.model.settings.settingsEnum.SettingsField;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

public class SettingsServiceTest {

    private static File testFile = new File("Settings_Example_For_Test.properties");
    private static File testFileTwo = new File("Settings_Example_For_Test_Two.properties");


    @BeforeClass
    public static void before() {
        SettingsService.createExampleSettings(testFile);
        PropertyService.writeProperty(testFile, SettingsField.ID_GOOGLE_SHEET, "any");
    }

    @Test
    public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<SettingsService> constructor = SettingsService.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void createExampleSettingsTest() {
        Assert.assertEquals(SettingsService.createExampleSettings(testFileTwo), true);
    }

    @Test
    public void createExampleSettingsTestTwo() {
        testFileTwo.setWritable(false);
        Assert.assertEquals(SettingsService.createExampleSettings(testFileTwo), true);
    }

    @Test
    public void getIsActiveTest() {
        PropertyService.writeProperty(testFile, SettingsField.ACTIVE_SETTINGS, "");
        Assert.assertTrue(!SettingsService.getIsActive(testFile));
    }

    @Test
    public void getIsActiveTestTwo() {
        PropertyService.writeProperty(testFile, SettingsField.ACTIVE_SETTINGS, "0");
        Assert.assertTrue(!SettingsService.getIsActive(testFile));
    }

    @Test
    public void getIsActiveTestThree() {
        PropertyService.writeProperty(testFile, SettingsField.ACTIVE_SETTINGS, "1");
        Assert.assertTrue(SettingsService.getIsActive(testFile));
    }

    @Test
    public void getIsActiveTestFour() {
        PropertyService.writeProperty(testFile, SettingsField.ACTIVE_SETTINGS, "true");
        Assert.assertTrue(SettingsService.getIsActive(testFile));
    }

    @Test
    public void getCompanyNameTest() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        Assert.assertEquals(SettingsService.getCompanyName(testFile), "Company");
    }

    @Test
    public void getCompanyNameTestTwo() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "");
        Assert.assertTrue(SettingsService.getCompanyName(testFile) == null);
    }

    @Test
    public void getFtpPathTest() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.FTP_PATH, "C:\\Home/FTP");
        Path path = new File("C:\\Home/FTP/Company").toPath();
        Assert.assertEquals(SettingsService.getFtpPath(testFile), path);
    }

    @Test
    public void getFtpPathTestTwo() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.FTP_PATH, "C:\\Home/FTP/");
        Path path = new File("C:\\Home/FTP/Company").toPath();
        Assert.assertEquals(SettingsService.getFtpPath(testFile), path);
    }

    @Test
    public void getFtpPathTestThree() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.FTP_PATH, "");
        Path path = new File("/Home/FTP/Company").toPath();
        Assert.assertTrue(SettingsService.getFtpPath(testFile) == null);
    }

    @Test
    public void getFtpPathTestFour() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.FTP_PATH, "/Home");
        Path path = new File("/Home/FTP/Company").toPath();
        Assert.assertTrue(SettingsService.getFtpPath(testFile) == null);
    }

    @Test
    public void getStoragePathTest() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home");
        Path path = new File("C:\\Home/Company").toPath();
        Assert.assertEquals(SettingsService.getStoragePath(testFile), path);
    }

    @Test
    public void getStorageTestTwo() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home/");
        Path path = new File("C:\\Home/Company").toPath();
        Assert.assertEquals(SettingsService.getStoragePath(testFile), path);
    }

    @Test
    public void getStoragePathTestThree() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "");
        Assert.assertTrue(SettingsService.getStoragePath(testFile) == null);
    }

    @Test
    public void getStoragePathTestFour() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "/Home");
        Path path = new File("/Home/FTP/Company").toPath();
        Assert.assertTrue(SettingsService.getStoragePath(testFile) == null);
    }

    @Test
    public void getHowManyBackupsToKeepTest() {
        PropertyService.writeProperty(testFile, SettingsField.HOW_MANY_BACKUPS_TO_KEEP, "2");
        int answer = 2;
        Assert.assertTrue(SettingsService.getHowManyBackupsToKeep(testFile) == answer);
    }

    @Test
    public void getHowManyBackupsToKeepTestTwo() {
        PropertyService.writeProperty(testFile, SettingsField.HOW_MANY_BACKUPS_TO_KEEP, "");
        int answer = 7;
        Assert.assertTrue(SettingsService.getHowManyBackupsToKeep(testFile) == answer);
    }

    @Test
    public void getHowManyBackupsToKeepTestThree() {
        PropertyService.writeProperty(testFile, SettingsField.HOW_MANY_BACKUPS_TO_KEEP, "dfds00sdfs");
        int answer = 7;
        Assert.assertTrue(SettingsService.getHowManyBackupsToKeep(testFile) == answer);
    }

    @Test
    public void getHowManyBackupsToKeepTestFour() {
        PropertyService.writeProperty(testFile, SettingsField.HOW_MANY_BACKUPS_TO_KEEP, "0");
        int answer = 7;
        Assert.assertTrue(SettingsService.getHowManyBackupsToKeep(testFile) == answer);
    }

    @Test
    public void getMinDateToArchiveTest() {
        PropertyService.writeProperty(testFile, SettingsField.MIN_DATE_TO_ARCHIVE, "5");
        int answer = 5;
        Assert.assertTrue(SettingsService.getMinDateToArchive(testFile) == answer);
    }

    @Test
    public void getMinDateToArchiveTestTwo() {
        PropertyService.writeProperty(testFile, SettingsField.MIN_DATE_TO_ARCHIVE, "");
        int answer = 2;
        Assert.assertTrue(SettingsService.getMinDateToArchive(testFile) == answer);
    }

    @Test
    public void getMinDateToArchiveTestThree() {
        PropertyService.writeProperty(testFile, SettingsField.MIN_DATE_TO_ARCHIVE, "dfds00sdfs");
        int answer = 2;
        Assert.assertTrue(SettingsService.getMinDateToArchive(testFile) == answer);
    }

    @Test
    public void getStorageBackupPathTest() {
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home");
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        Assert.assertEquals(SettingsService.getStorageBackupPath(testFile),
                "C:\\Home/Company/" + SettingsDefault.BACKUP_PATH_NAME.getStringValue());
    }

    @Test
    public void getStorageArchivePathTest() {
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home");
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        Assert.assertEquals(SettingsService.getStorageArchivePath(testFile),
                "C:\\Home/Company/" + SettingsDefault.BACKUP_ARCHIVE_PATH_NAME.getStringValue());
    }

    @Test
    public void getBuhPathTest() {
        Assert.assertEquals(SettingsService.getBuhPath(testFile),
                "C:\\Home/Company/" + SettingsDefault.BUH_PATH.getStringValue() + "Company");
    }

    @Test
    public void getBasePathTest() {
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home");
        Assert.assertEquals(SettingsService.getBasePath(testFile),
                "C:\\Home/Company/" + SettingsDefault.BASE_PATH.getStringValue());
    }

    @Test
    public void getUpdateLogPathTest() {
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home");
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        Assert.assertEquals(SettingsService.getUpdateLogPath(testFile),
                "C:\\Home/Company/" + SettingsDefault.UPDATE_LOG_PATH.getStringValue());
    }

    @Test
    public void getUfoFilesPathTest() {
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home");
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        Assert.assertEquals(SettingsService.getUfoFilesPath(testFile),
                "C:\\Home/Company/" + SettingsDefault.UFO_FILES_PATH.getStringValue());
    }

    @Test
    public void getBadFilesPathTest() {
        PropertyService.writeProperty(testFile, SettingsField.STORAGE_PATH, "C:\\Home");
        PropertyService.writeProperty(testFile, SettingsField.COMPANY_NAME, "Company");
        Assert.assertEquals(SettingsService.getBadFilesPath(testFile),
                "C:\\Home/Company/" + SettingsDefault.BAD_FILES_PATH.getStringValue());
    }

    @Test
    public void getSpreadSheetId() {
        Assert.assertEquals(SettingsService.getSpreadSheetId(testFile), "any");
    }


//    @Test
//    public void testWriteError() {
//        File fileTwo = new File("Settings_error.properties");
//        try {
//            fileTwo.createNewFile();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        PropertyService.writeProperty(fileTwo, SettingsField.ACTIVE_SETTINGS, "true");
//        PropertyService.writeProperty(fileTwo, SettingsField.MIN_DATE_TO_ARCHIVE, "0");
//        PropertyService.writeProperty(fileTwo, SettingsField.HOW_MANY_BACKUPS_TO_KEEP, "0");
//        PropertyService.writeProperty(fileTwo, SettingsField.COMPANY_NAME, "Company");
//        PropertyService.writeProperty(fileTwo, SettingsField.FTP_PATH, "C:\\");
//        PropertyService.writeProperty(fileTwo, SettingsField.STORAGE_PATH, "C:\\");
//        PropertyService.writeProperty(fileTwo, SettingsField.ID_GOOGLE_SHEET, "any");
//        PropertyService.writeProperty(fileTwo, SettingsField.HOW_OFTEN_CHECK_SHEETS, "g0h606.j1");
//        Settings settings = new Settings(fileTwo);
//    }

    @Test
    public void testTwo() {
        File file = new File("Settings_Komod.properties");
        String property = PropertyService.getProperty(file, SettingsField.MIN_DATE_TO_ARCHIVE);
        if (property == null || property.equals("") || !property.matches("^[1-9]+.*")) {
            PropertyService.writeProperty(file, SettingsField.MIN_DATE_TO_ARCHIVE,
                    "is not valid. The default value is set: "
                            + SettingsDefault.MIN_DATE_TO_ARCHIVE.getIntegerValue());
        }
        Matcher matcher = Pattern.compile("[1-9][0-9]*").matcher(property);
        matcher.find();
    }

    @Test
    public void testRegex() {
        String test = "D:\\";
        System.out.println(Pattern.compile("^[A-Z]{1}:\\\\.*$").matcher(test).matches());
    }

    @Test
    public void testThree() {
        File file = new File("Settings_Komod.properties");
        System.out.println(PropertyService.getProperty(file, SettingsField.STORAGE_PATH));
    }

    @Test
    public void testFour() {
        File file = new File("Settings_-Company-.properties");
        Settings settings = new Settings(file);
        System.out.println(settings.hashCode());
    }

    @Test
    public void testFive() {
        File file = new File("Settings_-Company-.properties");
        Assert.assertEquals(new Settings(file).hashCode(), new Settings(file).hashCode());
        System.out.println(new Settings(file).hashCode());
    }

    @Test
    public void testSix() {
        File file = new File("");
        System.out.println(file.getAbsoluteFile().getName());
    }

    @AfterClass
    public static void after() {
        try {
            Files.deleteIfExists(testFile.toPath());
            Files.deleteIfExists(testFileTwo.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
