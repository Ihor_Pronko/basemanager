package com.ihorp.bmanager.service.settings;

import com.ihorp.bmanager.model.exception.UndefinedFileException;
import com.ihorp.bmanager.service.file.UpdateLogFileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.time.LocalDate;

import static org.hamcrest.core.Is.is;

public class UpdateLogfileServiceTest {

    private File pawnExpertUpdateLogFile;
    private File baseManagerUpdateLogFile;
    private File newBaseManagerUpdateLogFile;

    @Before
    public void before() {
        pawnExpertUpdateLogFile = new File("up0021_170112.932");
        baseManagerUpdateLogFile = new File("Update 7 2017-02-14.txt");
        newBaseManagerUpdateLogFile = new File("Update №8 (8) Бердянск 2017-02-15.txt");
    }


    @Test
    public void findBranchCodeTest() {
        Assert.assertTrue(UpdateLogFileService.findBranchCode(pawnExpertUpdateLogFile) == 932);
    }

    @Test
    public void findBranchCodeTestTwo() {
        Assert.assertTrue(UpdateLogFileService.findBranchCode(baseManagerUpdateLogFile) == 7);
    }

    @Test
    public void findBranchCodeTestThree() {
        Assert.assertTrue(UpdateLogFileService.findBranchCode(newBaseManagerUpdateLogFile) == 8);
    }

    @Test
    public void findDateTest() throws UndefinedFileException {
        Assert.assertThat(UpdateLogFileService.findDate(pawnExpertUpdateLogFile), is(LocalDate.of(2017, 01, 12)));
    }

    @Test
    public void findDateTestTwo() throws UndefinedFileException {
        Assert.assertThat(UpdateLogFileService.findDate(baseManagerUpdateLogFile), is(LocalDate.of(2017, 02, 14)));
    }

    @Test
    public void findDateTestThree() throws UndefinedFileException {
        Assert.assertThat(UpdateLogFileService.findDate(newBaseManagerUpdateLogFile), is(LocalDate.of(2017, 02, 15)));
    }
}
