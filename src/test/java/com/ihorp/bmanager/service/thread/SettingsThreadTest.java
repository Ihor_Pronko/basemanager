package com.ihorp.bmanager.service.thread;

import com.ihorp.bmanager.model.settings.settingsEnum.SettingsDefault;
import org.junit.Test;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;

public class SettingsThreadTest {


    @Test
    public void testOne() {
        Arrays.stream(new File(Paths.get("").toAbsolutePath().toString()).listFiles())
                .filter(o -> o.getName().matches("Settings_.*\\.properties"))
                .filter(e -> !e.getName().matches(SettingsDefault.DEFAULT_SETTINGS_FILE_NAME.getStringValue()))
                .forEach(u -> System.out.println(u.getName()));
    }

}
